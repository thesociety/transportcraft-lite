![https://i.imgur.com/qKbnwNg.jpg](https://i.imgur.com/qKbnwNg.jpg)

Transportcraft Next Generation (formerly Transportcraft Lite) is a next-gen spiritual successor to [The Society - Transportcraft (Curseforge)](https://curseforge.com/minecraft/modpacks/the-society-transportcraft) that aims to be a bit more lightweight.

The aim of the modpack is to focus on transportation and tech. There aren't many config changes from out of the default as **it's mostly used on private server**.

Modpack is heavily based upon Create mod and mods that work well around it, but still keeping the original theme of transportation.
