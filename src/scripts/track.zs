import mods.create.SequencedAssemblyManager;

<recipetype:create:sequenced_assembly>.remove(<item:railways:track_phantom>);

<recipetype:create:sequenced_assembly>.addRecipe(<recipetype:create:sequenced_assembly>.builder("seq_blast_brick")
    .transitionTo(<item:railways:track_incomplete_phantom>) // what is it in middle
    .require(<item:minecraft:glass>) // start item
    .loops(1) // how many runs
    .addOutput(<item:railways:track_phantom>, 1) // what we actually create
    .addStep<mods.createtweaker.DeployerApplicationRecipe>((rb) => rb.require(<item:minecraft:iron_nugget>))
    .addStep<mods.createtweaker.DeployerApplicationRecipe>((rb) => rb.require(<item:minecraft:iron_nugget>))
    .addStep<mods.createtweaker.PressingRecipe>()
);
