# Used python-gitlab https://python-gitlab.readthedocs.io/
# Needed pip install --upgrade python-gitlab
import json
import os
import tomllib

import gitlab
import requests

# Check if required environment variables exist
gitlab_url = os.getenv("CI_SERVER_URL")
gitlab_token = os.getenv("PRIVATE_TOKEN") # CI_JOB_TOKEN didn't work for strange reason

if not gitlab_url:
    print("Missing gitlab_url env")
    exit(2)  # Error: Incorrect usage, such as invalid options or missing arguments

if not gitlab_token:
    print("Missing gitlab_token env")
    exit(2)  # Error: Incorrect usage, such as invalid options or missing arguments

gl = gitlab.Gitlab(gitlab_url, private_token=gitlab_token)
print("Connection to " + gitlab_url + " established")

if not gl:
    print("Connection to gitlab failed")
    exit(1)

gl_project = gl.projects.get(os.getenv("CI_PROJECT_ID"), lazy=True)
release = gl_project.releases.get(os.getenv("CI_COMMIT_REF_NAME"))

if not release:
    print("Release not found")
    exit(1)

changelog = release.description
modrinth_api_key = os.getenv("MODRINTH_API_KEY")
modrinth_project_id = os.getenv("MODRINTH_PROJECT_ID")

modpack_description = ""

modrinth_api = "https://api.modrinth.com/v2"

auth_header = {"Authorization": f"{modrinth_api_key}"}

mr_project = requests.get(modrinth_api + "/project/" + modrinth_project_id).json()

if not mr_project["id"]:
    print("No project")
    exit(1)

print("Updating modpack description")
# Generate modpack description
filenames = ["modpack-description.md", "src/modlist.md"]

for fname in filenames:
    with open(fname, encoding='UTF-8') as infile:
        for line in infile:
            modpack_description += line

if not modpack_description:
    print("No modpack description")
    exit(1)

# print(modpack_description)

# https://docs.modrinth.com/#tag/projects/operation/modifyProject
requests.patch(modrinth_api + "/project/" + modrinth_project_id, headers=auth_header, json={
    "body": modpack_description
})

print("Parsing modpack info")
# Get modpack info
with open("src/pack.toml", "rb") as f:
    modpack_info = tomllib.load(f)

print("Uploading modpack to MR")
file = "src/transportcraft-lite_" + os.getenv("CI_COMMIT_REF_NAME") + ".mrpack"

# Load the modpack file
with open(file, "rb") as f:
    files = {
        "file": f
    }

    print(files)

    data = {
        "name": modpack_info["name"] + " " + os.getenv("CI_COMMIT_REF_NAME"),
        "version_number": os.getenv("CI_COMMIT_REF_NAME"),
        "dependencies": [],
        "game_versions": [modpack_info["versions"]["minecraft"]],
        "changelog": changelog,
        "version_type": "release",
        "loaders": ["fabric"],
        "featured": False,
        "project_id": mr_project['id'],
        "file_parts": [file]
    }

    # print(json.dumps(data))

    # https://docs.modrinth.com/#tag/versions/operation/createVersion
    r = requests.post(modrinth_api + "/version", headers=auth_header, files=files, data={"data": json.dumps(data)})
    # print(r.request.headers)

    print(r.json())
