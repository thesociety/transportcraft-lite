import tomllib

print("Parsing modpack info")
# Get modpack info
with open("src/pack.toml", "rb") as f:
    modpack_info = tomllib.load(f)

path = "tc-lite"
print("Replacing file")


def replace_line_in_file(filename, search_string, new_line):
    with open(filename, 'r') as file:
        lines = file.readlines()

    with open(filename, 'w') as file:
        for line in lines:
            if line.startswith(search_string):
                file.write(new_line + '\n')
            else:
                file.write(line)


# Updating env file
replace_line_in_file(path + "/manifest/base/configs/env",
                     "VERSION=",
                     "VERSION=" + modpack_info["versions"]["minecraft"])
replace_line_in_file(path + "/manifest/base/configs/env",
                     "FABRIC_LOADER_VERSION="
                     , "FABRIC_LOADER_VERSION=" + modpack_info["versions"]["fabric"])

# Get packwiz job url
packwiz_package_job_url = open("packwiz_package_job_url", "r").read().replace('\n', '')

print(packwiz_package_job_url)

replace_line_in_file(path + "/manifest/base/configs/env",
                     "PACKWIZ_URL=",
                     "PACKWIZ_URL=" + str(packwiz_package_job_url) + "/artifacts/raw/src/pack.toml")

replace_line_in_file(path + "/data/server.properties",
                     "motd=",
                     "motd=" + modpack_info["name"] + " || " + modpack_info["version"])