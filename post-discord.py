import os
import tomllib
import discord
import gitlab

# # Check if required environment variables exist
gitlab_url = os.getenv("CI_SERVER_URL")
gitlab_token = os.getenv("PRIVATE_TOKEN")  # CI_JOB_TOKEN didn't work for strange reason

if not gitlab_url:
    print("Missing gitlab_url env")
    exit(2)  # Error: Incorrect usage, such as invalid options or missing arguments

if not gitlab_token:
    print("Missing gitlab_token env")
    exit(2)  # Error: Incorrect usage, such as invalid options or missing arguments

gl = gitlab.Gitlab(gitlab_url, private_token=gitlab_token)
print("Connection to " + gitlab_url + " established")

if not gl:
    print("Connection to gitlab failed")
    exit(1)

gl_project = gl.projects.get(os.getenv("CI_PROJECT_ID"), lazy=True)
release = gl_project.releases.get(os.getenv("CI_COMMIT_REF_NAME"))

if not release:
    print("Release not found")
    exit(1)

changelog = release.description

print("Parsing modpack info")
# Get modpack info
with open("src/pack.toml", "rb") as f:
    modpack_info = tomllib.load(f)

webhook_url = os.getenv("DISCORD_WEBOOK_URL")

if not webhook_url:
    print("Missing webhook url")
    exit(1)

dc_webhook = discord.SyncWebhook.from_url(webhook_url)

dc_embed = discord.Embed(
    title=modpack_info["name"] + " updated to " + modpack_info["version"],
    description="Changes:\n"
                + changelog + "\n"
                + "[Modrinth](https://modrinth.com/modpack/transportcraft-lite/version/"
                + modpack_info["version"]
                + ") | [Gitlab](https://gitlab.com/thesociety/transportcraft-lite/-/releases/"
                + modpack_info["version"]
                + ") | [Changelog](https://modrinth.com/modpack/transportcraft-lite/changelog)"
)

dc_webhook.send(embed=dc_embed)
