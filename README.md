[[_TOC_]]

# Repository
## Git LFS
> This repository uses Git LFS. You need to have it installed.

On Debian:
```
apt-get install git-lfs
```

On Windows: https://git-lfs.github.com/


## Pipeline
We are using https://gitlab.com/gitlab-ci-utils/gitlab-releaser to create releases automatically.

# Required tools
- **golang** 1.19 or higher: https://go.dev/doc/install
- **packwiz**
```
go install github.com/packwiz/packwiz@latest
```
- **packwiz-wrapper** (if you want to easily generate the modlist)
```
go install github.com/Merith-TK/packwiz-wrapper/cmd/pw@main
```

## Exporting versions
We are using packwiz to manage our modpack
https://packwiz.infra.link/tutorials/creating/getting-started/

### Exporting for development

```
cd src && packwiz mr export -y
```

# List of content (mods)
Mod list available here: [modlist.md](src/modlist.md)