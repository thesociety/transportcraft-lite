# Changelog

## [Unreleased]

## 2.11.1
- **Updated Fabric loader to version 0.16.10**
- Updated Advanced Backups: AdvancedBackups-fabric-1.20-3.6.4.jar -> AdvancedBackups-fabric-1.20-3.7.1.jar
- Updated Amendments: amendments-1.20-1.2.14-fabric.jar -> amendments-1.20-1.2.16-fabric.jar
- Updated Balm: balm-fabric-1.20.1-7.3.10.jar -> balm-fabric-1.20.1-7.3.14.jar
- Updated Cardinal Components API: cardinal-components-api-5.2.2.jar -> cardinal-components-api-5.2.3.jar
- Updated CC: Tweaked: cc-tweaked-1.20.1-fabric-1.114.2.jar --> cc-tweaked-1.20.1-fabric-1.114.4.jar
- Updated ChoiceTheorem's Overhauled Village: [fabric]ctov-3.4.10.jar -> [fabric]ctov-3.4.11.jar
- Updated Concurrent Chunk Management Engine (Fabric): c2me-fabric-mc1.20.1-0.2.0+alpha.11.13.jar -> c2me-fabric-mc1.20.1-0.2.0+alpha.11.15.jar
- Updated CraftTweaker: CraftTweaker-fabric-1.20.1-14.0.48.jar -> CraftTweaker-fabric-1.20.1-14.0.56.jar
- Updated Create Contraption Terminals: createcontraptionterminals-fabric-1.20-1.0.1.jar -> createcontraptionterminals-fabric-1.20-1.1.0.jar
- Updated Create Railways Navigator: createrailwaysnavigator-fabric-1.20.1-beta-0.7.1.jar -> createrailwaysnavigator-fabric-1.20.1-beta-0.7.2.jar
- Updated Create: Copycats+: copycats-2.1.4+mc.1.20.1-fabric.jar -> copycats-2.2.0+mc.1.20.1-fabric.jar
- Updated Create: Garnished: garnished-2.0.2+1.20.1-fabric.jar -> garnished-2.0.7+1.20.1-fabric.jar
- Updated Create: Metalwork: create-metalwork-1.20.1-1.0.8-fabric.jar -> create-metalwork-1.20.1-1.0.10-fabric.jar
- Updated CreateTweaker: CreateTweaker-fabric-1.20.1-4.0.13.jar -> CreateTweaker-fabric-1.20.1-4.0.15.jar
- Updated Customizable Player Models: CustomPlayerModels-Fabric-1.20-0.6.19a.jar -> CustomPlayerModels-Fabric-1.20-0.6.20a.jar
- Updated EMI Loot: emi_loot-0.7.4+1.20.1+fabric.jar -> emi_loot-0.7.5+1.20.1+fix1+fabric.jar
- Updated EMI: emi-1.1.18+1.20.1+fabric.jar -> emi-1.1.19+1.20.1+fabric.jar
- Updated Fabric Language Kotlin: fabric-language-kotlin-1.13.0+kotlin.2.1.0.jar -> fabric-language-kotlin-1.13.1+kotlin.2.1.10.jar
- Updated Farmer's Delight Refabricated: FarmersDelight-1.20.1-2.2.5+refabricated.jar -> FarmersDelight-1.20.1-2.2.6+refabricated.jar
- Updated Fusion (Connected Textures): fusion-1.1.1-fabric-mc1.20.1.jar -> fusion-1.2.3-fabric-mc1.20.1.jar
- Updated Fzzy Config: fzzy_config-0.6.0+1.20.1.jar -> fzzy_config-0.6.4+1.20.1.jar
- Updated ImmediatelyFast: ImmediatelyFast-Fabric-1.3.3+1.20.4.jar -> ImmediatelyFast-Fabric-1.3.4+1.20.4.jarr
- Updated Jade 🔍: Jade-1.20-Fabric-11.12.0.jar -> Jade-1.20-Fabric-11.12.3.jar
- Updated Just Enough Items: jei-1.20.1-fabric-15.20.0.105.jar -> jei-1.20.1-fabric-15.20.0.106.jar
- Updated Lithium: lithium-fabric-mc1.20.1-0.11.2.jar -> lithium-fabric-mc1.20.1-0.11.3.jar
- Updated Lithostitched: lithostitched-fabric-1.20.1-1.3.10.jar -> lithostitched-fabric-1.20.1-1.4.2.jar
- Updated Macaw's Doors: mcw-doors-1.1.1fabric-mc1.20.1.jar -> mcw-doors-1.1.2-mc1.20.1fabric.jar
- Updated ModernFix: modernfix-fabric-5.20.0+mc1.20.1.jar -> modernfix-fabric-5.20.2+mc1.20.1.jar
- Updated Moonlight Lib: moonlight-1.20-2.13.46-fabric.jar -> moonlight-1.20-2.13.58-fabric.jar
- Updated Patchouli: Patchouli-1.20.1-84-FABRIC.jar -> Patchouli-1.20.1-84.1-FABRIC.jar
- Updated Remote Player Waypoints for Xaero's Map: remote_player_waypoints_for_xaero-fabric-3.2.1-1.20-1.20.1.jar -> remote_player_waypoints_for_xaero-fabric-3.2.3-1.20-1.20.1.jar
- Updated SuperMartijn642's Core Lib: supermartijn642corelib-1.1.18-fabric-mc1.20.1.jar -> supermartijn642corelib-1.1.18a-fabric-mc1.20.1.jar
- Updated Supplementaries: supplementaries-1.20-3.1.10-fabric.jar -> supplementaries-1.20-3.1.12-fabric.jar
- Updated Tom's Simple Storage Mod: toms_storage_fabric-1.20-1.6.9.jar -> toms_storage_fabric-1.20-1.7.0.jar
- Updated Traveler's Backpack: travelersbackpack-fabric-1.20.1-9.1.18.jar -> travelersbackpack-fabric-1.20.1-9.1.25.jar
- Updated Xaero's Minimap: Xaeros_Minimap_24.7.1_Fabric_1.20.jar -> Xaeros_Minimap_25.0.0_Fabric_1.20.jar

## 2.10.0
- **Added Immersive Optimization**
- **Added Create: Optimization**
- **Removed Create Track Map** (mod seems to be abandoned, breaks with other mods, unreplied issues, ignored pull requests no commits for 2 years)
- **Replaced Create: Diesel Generators [Fabric] with Create: Diesel Generators [Fabric Restitched]**
- Updated Amendments: amendments-1.20-1.2.12-fabric.jar -> amendments-1.20-1.2.14-fabric.jar
- Updated Bad Wither No Cookie - Reloaded: bwncr-fabric-1.20.1-3.17.1.jar -> bwncr-fabric-1.20.1-3.17.2.jar
- Updated Balm: balm-fabric-1.20.1-7.3.9.jar -> balm-fabric-1.20.1-7.3.10.jar
- Updated CraftTweaker: CraftTweaker-fabric-1.20.1-14.0.44.jar -> CraftTweaker-fabric-1.20.1-14.0.48.jar
- Updated Create Contraption Terminals: createcontraptionterminals-fabric-1.20-1.0.0.jar -> createcontraptionterminals-fabric-1.20-1.0.1.jar
- Updated Create Fabric: create-fabric-0.5.1-j-build.1604+mc1.20.1.jar -> create-fabric-0.5.1-j-build.1631+mc1.20.1.jar
- Updated Create Jetpack: create_jetpack-fabric-4.2.1.jar -> create_jetpack-fabric-4.3.0.jar
- Updated Create Railways Navigator: createrailwaysnavigator-fabric-1.20.1-0.6.0.jar -> createrailwaysnavigator-fabric-1.20.1-beta-0.7.1.jar
- Updated Create: Steam 'n' Rails: Steam_Rails-1.6.7+fabric-mc1.20.1.jar -> Steam_Rails-1.6.9+fabric-mc1.20.1.jar
- Updated EMI Ores: emi_ores-1.0+1.20.1+fabric.jar -> emi_ores-1.2+1.20.1+fabric.jar
- Updated Fabric API: fabric-api-0.92.2+1.20.1.jar -> fabric-api-0.92.3+1.20.1.jar
- Updated Fabric Language Kotlin: fabric-language-kotlin-1.12.1+kotlin.2.0.20.jar -> fabric-language-kotlin-1.13.0+kotlin.2.1.0.jar
- Updated Farmer's Delight Refabricated: FarmersDelight-1.20.1-2.2.0+refabricated.jar -> FarmersDelight-1.20.1-2.2.5+refabricated.jar
- Updated Forgotten Graves: forgottengraves-3.2.15+1.20.1.jar -> forgottengraves-3.2.18+1.20.1.jar
- Updated Fzzy Config: fzzy_config-0.5.8+1.20.1.jar -> fzzy_config-0.6.0+1.20.1.jar
- Updated Geckolib: geckolib-fabric-1.20.1-4.4.9.jar -> geckolib-fabric-1.20.1-4.7.jar
- Updated ImmediatelyFast: ImmediatelyFast-Fabric-1.3.2+1.20.4.jar -> ImmediatelyFast-Fabric-1.3.3+1.20.4.jar
- Updated Immersive Aircraft: immersive_aircraft-1.1.5+1.20.1-fabric.jar -> immersive_aircraft-1.1.8+1.20.1-fabric.jar
- Updated Jade Addons (Fabric): JadeAddons-1.20.1-Fabric-5.3.1.jar -> JadeAddons-1.20.1-Fabric-5.4.0.jar
- Updated Lithostitched: lithostitched-fabric-1.20.1-1.3.8.jar -> lithostitched-fabric-1.20.1-1.3.10.jar
- Updated ModernFix: modernfix-fabric-5.19.5+mc1.20.1.jar -> modernfix-fabric-5.20.0+mc1.20.1.jar
- Updated Moonlight Lib: moonlight-1.20-2.13.21-fabric.jar -> moonlight-1.20-2.13.46-fabric.jar
- Updated Polymorph: polymorph-fabric-0.49.7+1.20.1.jar -> polymorph-fabric-0.49.8+1.20.1.jar
- Updated SuperMartijn642's Core Lib: supermartijn642corelib-1.1.17a-fabric-mc1.20.1.jar -> supermartijn642corelib-1.1.18-fabric-mc1.20.1.jar
- Updated Supplementaries: supplementaries-1.20-3.0.7-fabric.jar -> supplementaries-1.20-3.1.10-fabric.jar
- Updated Tom's Simple Storage Mod: toms_storage_fabric-1.20-1.6.8.jar -> toms_storage_fabric-1.20-1.6.9.jar
- Updated Xaero's Minimap: Xaeros_Minimap_24.6.1_Fabric_1.20.jar -> Xaeros_Minimap_24.7.1_Fabric_1.20.jar
- Updated Xaero's World Map: XaerosWorldMap_1.39.0_Fabric_1.20.jar -> XaerosWorldMap_1.39.2_Fabric_1.20.jar
- Updated YetAnotherConfigLib: YetAnotherConfigLib-3.6.1+1.20.1-fabric.jar -> YetAnotherConfigLib-3.6.2+1.20.1-fabric.jar

## 2.9.0
- **Updated Fabric loader to version 0.16.9**
- **Updated Create Fabric: create-fabric-0.5.1-f-build.1417+mc1.20.1.jar -> create-fabric-0.5.1-j-build.1604+mc1.20.1.jar**
- Updated CC: Tweaked: cc-tweaked-1.20.1-fabric-1.114.0.jar -> cc-tweaked-1.20.1-fabric-1.114.2.jar
- Updated Create: Steam 'n' Rails: Steam_Rails-1.6.4+fabric-mc1.20.1.jar -> Steam_Rails-1.6.7+fabric-mc1.20.1.jar
- Updated Forgotten Graves: forgottengraves-3.2.14+1.20.1.jar -> forgottengraves-3.2.15+1.20.1.jar
- Updated Fzzy Config: fzzy_config-0.5.7+1.20.1.jar -> fzzy_config-0.5.8+1.20.1.jar
- Updated Lithostitched: lithostitched-fabric-1.20.1-1.3.4.jar -> lithostitched-fabric-1.20.1-1.3.8.jar
- **Removed Modern Train Parts** (not updated in a while, not open-source, no issue reports etc)
- **Removed Create: Design n' Decor** (seems dead mod, no activity on repos for 8 months, no repling to sisues etc)

## 2.8.7.2
- Reverted Moonlight Lib to moonlight-1.20-2.13.21-fabric.jar due to server crash
- Reverted Supplementaries to supplementaries-1.20-3.0.7-fabric.jar due to server crash

## 2.8.7.1
- Reverted Create: TrainUtilities to trainutilities-fabric-2.2.0.jar due to server crash: https://github.com/ProgrammerLP/Create-Trainutilities/issues/7 (again)

## 2.8.7
- Updated Advanced Backups: AdvancedBackups-fabric-1.20-3.6.3.jar -> AdvancedBackups-fabric-1.20-3.6.4.jar
- Updated CC: Tweaked: cc-tweaked-1.20.1-fabric-1.113.1.jar -> cc-tweaked-1.20.1-fabric-1.114.0.jar
- Updated ChoiceTheorem's Overhauled Village: [Fabric]CTOV-3-4-9a.jar -> [fabric]ctov-3.4.10.jar
- Updated CraftTweaker: CraftTweaker-fabric-1.20.1-14.0.43.jar -> CraftTweaker-fabric-1.20.1-14.0.44.jar
- Updated Create Jetpack: create_jetpack-fabric-4.2.0.jar -> create_jetpack-fabric-4.2.1.jar
- Updated Create: Garnished: garnished-2.0.1+1.20.1-fabric.jar -> garnished-2.0.2+1.20.1-fabric.jar
- Updated Create: Metalwork: create-metalwork-1.20.1-1.0.7-fabric.jar -> create-metalwork-1.20.1-1.0.8-fabric.jar
- Updated Create: TrainUtilities: trainutilities-fabric-2.2.0.jar -> trainutilities-fabric-2.3.0.jar
- Updated Farmer's Delight Refabricated: FarmersDelight-1.20.1-2.1.6+refabricated.jar -> FarmersDelight-1.20.1-2.2.0+refabricated.jar
- Updated Forge Config API Port: ForgeConfigAPIPort-v8.0.0-1.20.1-Fabric.jar -> ForgeConfigAPIPort-v8.0.1-1.20.1-Fabric.jar
- Updated Fzzy Config: fzzy_config-0.5.6+1.20.1.jar -> fzzy_config-0.5.7+1.20.1.jar
- Updated ImmediatelyFast: ImmediatelyFast-Fabric-1.3.1+1.20.4.jar -> ImmediatelyFast-Fabric-1.3.2+1.20.4.jar
- Updated Immersive Aircraft: immersive_aircraft-1.1.2+1.20.1-fabric.jar -> immersive_aircraft-1.1.5+1.20.1-fabric.jar
- Updated ModernFix: modernfix-fabric-5.19.4+mc1.20.1.jar -> modernfix-fabric-5.19.5+mc1.20.1.jar
- Updated Moonlight Lib: moonlight-1.20-2.13.21-fabric.jar -> moonlight-1.20-2.13.28-fabric.jar
- Updated Puzzles Lib: PuzzlesLib-v8.1.24-1.20.1-Fabric.jar -> PuzzlesLib-v8.1.25-1.20.1-Fabric.jar
- Updated Remote Player Waypoints for Xaero's Map: remote_player_waypoints_for_xaero-fabric-3.2.0-1.20-1.20.1.jar -> remote_player_waypoints_for_xaero-fabric-3.2.1-1.20-1.20.1.jar
- Updated Supplementaries: supplementaries-1.20-3.0.7-fabric.jar -> supplementaries-1.20-3.1.8-fabric.jar

## 2.8.6.1
- Reverted Create: TrainUtilities to trainutilities-fabric-2.2.0.jar due to server crash: https://github.com/ProgrammerLP/Create-Trainutilities/issues/7

## 2.8.6
- Updated Collective: collective-1.20.1-7.84.jar -> collective-1.20.1-7.87.jar
- Updated Create: TrainUtilities: trainutilities-fabric-2.2.0.jar -> trainutilities-fabric-2.3.0.jar
- Updated CreateTweaker: CreateTweaker-fabric-1.20.1-4.0.12.jar -> CreateTweaker-fabric-1.20.1-4.0.13.jar
- Updated EMI: emi-1.1.16+1.20.1+fabric.jar -> emi-1.1.18+1.20.1+fabric.jar
- Updated Fzzy Config: fzzy_config-0.5.4+1.20.1.jar -> fzzy_config-0.5.6+1.20.1.jar
- Updated ImmediatelyFast: ImmediatelyFast-Fabric-1.2.21+1.20.4.jar -> ImmediatelyFast-Fabric-1.3.1+1.20.4.jar
- Updated Just Enough Items: jei-1.20.1-fabric-15.20.0.104.jar -> jei-1.20.1-fabric-15.20.0.105.jar
- Updated Lithostitched: lithostitched-fabric-1.20.1-1.3.1b.jar -> lithostitched-fabric-1.20.1-1.3.4.jar
- Updated Moonlight Lib: moonlight-1.20-2.13.13-fabric.jar -> moonlight-1.20-2.13.21-fabric.jar
- Updated Supplementaries: supplementaries-1.20-2.8.17-fabric.jar -> supplementaries-1.20-3.0.7-fabric.jar
- Updated Xaero's Minimap: Xaeros_Minimap_24.5.0_Fabric_1.20.jar -> Xaeros_Minimap_24.6.1_Fabric_1.20.jar
- Updated YetAnotherConfigLib: YetAnotherConfigLib-3.6.0+1.20.1-fabric.jar -> YetAnotherConfigLib-3.6.1+1.20.1-fabric.jar

## 2.8.5.1
- Fix not setting correct Fabric Kotlin version

## 2.8.5
- Downgraded Fabric Kotlin back to 1.12.1+kotlin.2.0.20 (because of server crash loop issue https://github.com/FabricMC/fabric-language-kotlin/issues/150 & https://github.com/jenchanws/create-track-map/issues/76)

## 2.8.4
- Downgraded Create: TrainUtilities to trainutilities-fabric-2.2.0.jar

## 2.8.3
- **Updated Fabric loader to version 0.16.7**
- Updated Amendments: amendments-1.20-1.2.11-fabric.jar -> amendments-1.20-1.2.12-fabric.jar
- Updated Bad Wither No Cookie - Reloaded: bwncr-fabric-1.20.1-3.17.0.jar -> bwncr-fabric-1.20.1-3.17.1.jar
- Updated Chipped: Chipped-fabric-1.20.1-3.0.6.jar -> chipped-fabric-1.20.1-3.0.7.jar
- Updated Create Railways Navigator: createrailwaysnavigator-fabric-1.20.1-0.5.5.jar -> createrailwaysnavigator-fabric-1.20.1-0.6.0.jar
- Updated Create: Garnished: garnished-1.9.8+1.20.1-fabric.jar -> garnished-2.0.1+1.20.1-fabric.jar
- Updated Create: TrainUtilities: trainutilities-fabric-2.2.0.jar -> trainutilities-fabric-2.3.0.jar
- Updated EMI Ores: emi_ores-0.4+1.20.1.jar -> emi_ores-1.0+1.20.1+fabric.jar
- Updated Fabric Language Kotlin: fabric-language-kotlin-1.12.1+kotlin.2.0.20.jar -> fabric-language-kotlin-1.12.3+kotlin.2.0.21.jar
- Updated Fzzy Config: fzzy_config-0.5.2+1.20.1.jar -> fzzy_config-0.5.4+1.20.1.jar
- Updated Lithostitched: lithostitched-fabric-1.20.1-1.3.1a.jar -> lithostitched-fabric-1.20.1-1.3.1b.jar
- Updated Moonlight Lib: moonlight-1.20-2.13.7-fabric.jar -> moonlight-1.20-2.13.13-fabric.jar
- Updated Traveler's Backpack: travelersbackpack-fabric-1.20.1-9.1.17.jar -> travelersbackpack-fabric-1.20.1-9.1.18.jar
- Updated YetAnotherConfigLib: YetAnotherConfigLib-3.5.0+1.20.1-fabric.jar -> YetAnotherConfigLib-3.6.0+1.20.1-fabric.jar

## 2.8.1
- **Downgraded Fabric loader to 0.16.5** - latest version just stops during client launching

## 2.8.0
- **Updated Fabric loader to version 0.16.6**
- Added LambDynamicLights (lambdynamiclights-2.3.2+1.20.1.jar)
- Added Create: Dynamic Lights (create-dyn-light-fabric1.20.1+1.0.2.jar)
- Updated EMI: emi-1.1.14+1.20.1+fabric.jar -> emi-1.1.16+1.20.1+fabric.jar
- Updated Fzzy Config: fzzy_config-0.5.1+1.20.1.jar -> fzzy_config-0.5.2+1.20.1.jar
- Updated Just Enough Items: jei-1.20.1-fabric-15.20.0.101.jar -> jei-1.20.1-fabric-15.20.0.104.jar
- Updated Moonlight Lib: moonlight-1.20-2.13.2-fabric.jar -> moonlight-1.20-2.13.7-fabric.jar
- Updated Puzzles Lib: PuzzlesLib-v8.1.23-1.20.1-Fabric.jar -> PuzzlesLib-v8.1.24-1.20.1-Fabric.jar

## 2.7.3-s1
- Fix server getting invalid variables because we replace them too early

## 2.7.3
- Pipeline fixes (possibly some fixes withing modpack files due to it also)

## 2.7.2
- Changed the file we read current release from

## 2.7.1
- Reverted Fabric Kotlin back to 1.12.1+kotlin.2.0.20 (because of server crash loop issue https://github.com/FabricMC/fabric-language-kotlin/issues/150 & https://github.com/jenchanws/create-track-map/issues/76)

## 2.7.0
- **Updated Fabric loader to version 0.16.5**
- Added:
  - Project "Distant Horizons" successfully added! (DistantHorizons-2.2.1-a-1.20.1-forge-fabric.jar)
  - Project "Create: Less Ticking" successfully added! (create_lt-1.0.1-fabric-mc1.20.1.jar)
  - Fabric override for Create: Interiors (https://github.com/aesefficio/CreateInteriorsMod/issues/23)

- Removed:
  - Bobby

- Updated:
  - Amendments: amendments-1.20-1.2.8-fabric.jar -> amendments-1.20-1.2.11-fabric.jar
  - CC: Tweaked: cc-tweaked-1.20.1-fabric-1.113.0.jar -> cc-tweaked-1.20.1-fabric-1.113.1.jar
  - ChoiceTheorem's Overhauled Village: [Fabric]CTOV-v3.4.6.jar -> [Fabric]CTOV-3-4-9a.jar
  - Cloth Config API: cloth-config-11.1.118-fabric.jar -> cloth-config-11.1.136-fabric.jar
  - Companion 🐕: Companion-1.20.1-fabric-5.2.0.jar -> Companion-1.20.1-Fabric-5.2.2.jar
  - Create Slice & Dice: sliceanddice-fabric-3.2.3.jar -> sliceanddice-fabric-3.3.1.jar
  - Create: Copycats+: copycats-2.1.2+mc.1.20.1-fabric.jar -> copycats-2.1.4+mc.1.20.1-fabric.jar
  - Create: Interiors: interiors-0.5.3+fabric-mc1.20.1.jar -> interiors-0.5.6+fabric-mc1.20.1-build.104.jar
  - CreateTweaker: CreateTweaker-fabric-1.20.1-4.0.10.jar -> CreateTweaker-fabric-1.20.1-4.0.12.jar
  - Customizable Player Models: CustomPlayerModels-Fabric-1.20-0.6.17b.jar -> CustomPlayerModels-Fabric-1.20-0.6.19a.jar
  - EMI Loot: emi_loot-0.6.6+1.20.1.jar -> emi_loot-0.7.4+1.20.1+fabric.jar
  - EMI: emi-1.1.12+1.20.1+fabric.jar -> emi-1.1.14+1.20.1+fabric.jar
  - Fabric Language Kotlin: fabric-language-kotlin-1.12.1+kotlin.2.0.20.jar -> fabric-language-kotlin-1.12.2+kotlin.2.0.20.jar
  - Farmer's Delight Refabricated: FarmersDelight-1.20.1-2.1.4+refabricated.jar -> FarmersDelight-1.20.1-2.1.6+refabricated.jar
  - Forgotten Graves: forgottengraves-3.2.13+1.20.1.jar -> forgottengraves-3.2.14+1.20.1.jar
  - Immersive Aircraft: immersive_aircraft-1.0.1+1.20.1-fabric.jar -> immersive_aircraft-1.1.2+1.20.1-fabric.jar
  - Jade 🔍: Jade-1.20-fabric-11.11.0.jar -> Jade-1.20-Fabric-11.12.0.jar
  - Just Enough Items: jei-1.20.1-fabric-15.14.0.64.jar -> jei-1.20.1-fabric-15.20.0.101.jar
  - Lithostitched: lithostitched-fabric-1.20.1-1.1.8.jar -> lithostitched-fabric-1.20.1-1.3.1a.jar
  - Moonlight Lib: moonlight-1.20-2.12.17-fabric.jar -> moonlight-1.20-2.13.2-fabric.jar
  - NiftyCarts: niftycarts-3.0.2+1.20.1.jar -> niftycarts-20.1.3.jar
  - Polymorph: polymorph-fabric-0.49.5+1.20.1.jar -> polymorph-fabric-0.49.7+1.20.1.jar
  - Puzzles Lib: PuzzlesLib-v8.1.22-1.20.1-Fabric.jar -> PuzzlesLib-v8.1.23-1.20.1-Fabric.jar
  - Tectonic: tectonic-fabric-1.20.1-2.4.1.jar -> tectonic-fabric-1.20.1-2.4.1b.jar
  - Traveler's Backpack: travelersbackpack-fabric-1.20.1-9.1.16.jar -> travelersbackpack-fabric-1.20.1-9.1.17.jar
  - Where Is It: whereisit-2.4.3+1.20.1.jar -> whereisit-2.6.3+1.20.1.jar
  - Xaero's Minimap: Xaeros_Minimap_24.3.0_Fabric_1.20.jar -> Xaeros_Minimap_24.5.0_Fabric_1.20.jar
  - YUNG's API: YungsApi-1.20-Fabric-4.0.5.jar -> YungsApi-1.20-Fabric-4.0.6.jar

## 2.6.2
- Updated Advanced Backups: AdvancedBackups-fabric-1.20-3.6.jar -> AdvancedBackups-fabric-1.20-3.6.3.jar
- Updated Balm: balm-fabric-1.20.1-7.3.6.jar -> balm-fabric-1.20.1-7.3.9.jar
- Updated Building Wands: BuildingWands-mc1.20.1-2.6.9-release.jar -> BuildingWands-mc1.20.1-2.7-fabric-beta.jar
- Updated CC: Tweaked: cc-tweaked-1.20.1-fabric-1.112.0.jar -> cc-tweaked-1.20.1-fabric-1.113.0.jar
- Updated Collective: collective-1.20.1-7.81.jar -> collective-1.20.1-7.84.jar
- Updated Concurrent Chunk Management Engine (Fabric): c2me-fabric-mc1.20.1-0.2.0+alpha.11.10.jar -> c2me-fabric-mc1.20.1-0.2.0+alpha.11.13.jar
- Updated CraftTweaker: CraftTweaker-fabric-1.20.1-14.0.40.jar -> CraftTweaker-fabric-1.20.1-14.0.43.jar
- Updated Create: Garnished: garnished-1.9.5+1.20.1-fabric.jar -> garnished-1.9.8+1.20.1-fabric.jar
- Updated Create: Metalwork: create-metalwork-1.20.1-1.0.6-fabric.jar -> create-metalwork-1.20.1-1.0.7-fabric.jar
- Updated Create: Train Perspective: create-train-perspective-0.6.0+mc1.20.1.jar -> create-train-perspective-1.0.0+mc1.20.1.jar
- Updated Create: TrainUtilities: trainutilities-fabric-2.1.1.jar -> trainutilities-fabric-2.2.0.jar
- Updated Eco Stack Manager: eco_stack_manager-fabric-1.20.1-1.2.0.jar -> eco_stack_manager-fabric-1.20.1-1.3.0.jar
- Updated EMI: emi-1.1.11+1.20.1+fabric.jar -> emi-1.1.12+1.20.1+fabric.jar
- Updated Enchanted Vertical Slabs: enchanted-vertical-slabs-2.2-fabric-mc1.20.1.jar -> enchanted-vertical-slabs-2.2.1-backport1.20.1-fabric-mc1.20.1.jar
- Updated Fabric Language Kotlin: fabric-language-kotlin-1.12.0+kotlin.2.0.10.jar -> fabric-language-kotlin-1.12.1+kotlin.2.0.20.jar
- Updated Farmer's Delight Refabricated: FarmersDelight-1.20.1-2.1.2+refabricated.jar -> FarmersDelight-1.20.1-2.1.4+refabricated.jar
- Updated Geckolib: geckolib-fabric-1.20.1-4.4.7.jar -> geckolib-fabric-1.20.1-4.4.9.jar
- Updated ImmediatelyFast: ImmediatelyFast-Fabric-1.2.19+1.20.4.jar -> ImmediatelyFast-Fabric-1.2.21+1.20.4.jar
- Updated Jade 🔍: Jade-1.20-fabric-11.10.0.jar -> Jade-1.20-fabric-11.11.0.jar
- Updated Just Enough Items: jei-1.20.1-fabric-15.12.1.46.jar -> jei-1.20.1-fabric-15.14.0.64.jar
- Updated ModernFix: modernfix-fabric-5.19.0+mc1.20.1.jar -> modernfix-fabric-5.19.4+mc1.20.1.jar
- Updated Moonlight Lib: moonlight-1.20-2.12.10-fabric.jar -> moonlight-1.20-2.12.17-fabric.jar
- Updated Noisium: noisium-fabric-2.2.2+mc1.20-1.20.1.jar -> noisium-fabric-2.3.0+mc1.20-1.20.1.jar
- Updated Puzzles Lib: PuzzlesLib-v8.1.21-1.20.1-Fabric.jar -> PuzzlesLib-v8.1.22-1.20.1-Fabric.jar
- Updated Remote Player Waypoints for Xaero's Map: remote_player_waypoints_for_xaero-fabric-3.1.0-1.20-1.20.1.jar -> remote_player_waypoints_for_xaero-fabric-3.2.0-1.20-1.20.1.jar
- Updated Resourceful Lib: resourcefullib-fabric-1.20.1-2.1.28.jar -> resourcefullib-fabric-1.20.1-2.1.29.jar
- Updated SuperMartijn642's Core Lib: supermartijn642corelib-1.1.17-fabric-mc1.20.1.jar -> supermartijn642corelib-1.1.17a-fabric-mc1.20.1.jar
- Updated Supplementaries: supplementaries-1.20-2.8.17-fabric.jar -> supplementaries-1.20-2.8.17-fabric.jar
- Updated Tectonic: tectonic-fabric-1.20-2.3.5b.jar -> tectonic-fabric-1.20.1-2.4.1.jar
- Updated Xaero's Minimap: Xaeros_Minimap_24.2.0_Fabric_1.20.jar -> Xaeros_Minimap_24.3.0_Fabric_1.20.jar
- Updated Xaero's World Map: XaerosWorldMap_1.38.8_Fabric_1.20.jar -> XaerosWorldMap_1.39.0_Fabric_1.20.jar

## 2.6.1
- Actually add Create Contraption Terminals
- Removed Actually Harvest and readded RightClickHarvest and it's required mods

## 2.6.0
- **Removed RightClickHarvest and JamLib**
- **Removed RightClickHarvest Supplementaries Compat**
- **Added Actually Harvest**
- **Added Modern Train Parts**
- **Added Create: TrainUtilities**
- **Added Create Contraption Terminals**
- Updated Collective: collective-1.20.1-7.80.jar -> collective-1.20.1-7.81.jar
- Updated Create: Train Perspective: create-train-perspective-0.5.0+mc1.20.1.jar -> create-train-perspective-0.6.0+mc1.20.1.jar
- Updated EMI: emi-1.1.10+1.20.1+fabric.jar -> emi-1.1.11+1.20.1+fabric.jar
- Updated ImmediatelyFast: ImmediatelyFast-Fabric-1.2.18+1.20.4.jar -> ImmediatelyFast-Fabric-1.2.19+1.20.4.jar
- Updated Just Enough Items: jei-1.20.1-fabric-15.11.0.43.jar -> jei-1.20.1-fabric-15.12.1.46.jar
- Updated Noisium: noisium-fabric-2.0.3+mc1.20-1.20.1.jar -> noisium-fabric-2.2.2+mc1.20-1.20.1.jar

## 2.5.14
- **Updated Fabric loader to version 0.16.0**
- Updated CC: Tweaked: cc-tweaked-1.20.1-fabric-1.111.0.jar -> cc-tweaked-1.20.1-fabric-1.112.0.jar
- Updated Collective: collective-1.20.1-7.71.jar -> collective-1.20.1-7.80.jar
- Updated Companion 🐕: Companion-1.20-fabric-5.1.0.jar -> Companion-1.20.1-fabric-5.2.0.jar
- Updated Concurrent Chunk Management Engine (Fabric): c2me-fabric-mc1.20.1-0.2.0+alpha.11.7.jar -> c2me-fabric-mc1.20.1-0.2.0+alpha.11.10.jar
- Updated Create: Copycats+: copycats-2.0.5+mc.1.20.1-fabric.jar -> copycats-2.1.2+mc.1.20.1-fabric.jar
- Updated Create: Design n' Decor: design_decor-0.4_fabric+1.20.1.jar -> design_decor-0.4.0b_fabric+1.20.1.jar
- Updated Create: Garnished: garnished-1.8.7+1.20.1-fabric.jar -> garnished-1.9.5+1.20.1-fabric.jar
- Updated Enchanted Vertical Slabs: enchanted-vertical-slabs-1.9.1.jar -> enchanted-vertical-slabs-2.2-fabric-mc1.20.1.jar
- Updated Enderman Tweaks: enderman_tweaks-0.3.1.jar -> enderman_tweaks-0.4.0.jar
- Updated Fabric Language Kotlin: fabric-language-kotlin-1.11.0+kotlin.2.0.0.jar -> fabric-language-kotlin-1.12.0+kotlin.2.0.10.jar
- Updated Farmer's Delight Refabricated: FarmersDelight-1.20.1-2.1.1+refabricated.jar -> FarmersDelight-1.20.1-2.1.2+refabricated.jar
- Updated Forgotten Graves: forgottengraves-3.2.11+1.20.1.jar -> forgottengraves-3.2.13+1.20.1.jar
- Updated Jade Addons (Fabric): JadeAddons-1.20.1-fabric-5.2.6.jar -> JadeAddons-1.20.1-Fabric-5.3.1.jar
- Updated Just Enough Items: jei-1.20.1-fabric-15.8.0.16.jar -> jei-1.20.1-fabric-15.11.0.43.jar
- Updated Lithostitched: lithostitched-fabric-1.20.1-1.1.6.jar -> lithostitched-fabric-1.20.1-1.1.8.jar
- Updated Macaw's Windows: mcw-windows-2.2.1-mc1.20.1fabric.jar -> mcw-windows-2.3.0-mc1.20.1fabric.jar
- Updated ModernFix: modernfix-fabric-5.18.10+mc1.20.1.jar -> modernfix-fabric-5.19.0+mc1.20.1.jar
- Updated Moonlight Lib: moonlight-1.20-2.12.8-fabric.jar -> moonlight-1.20-2.12.10-fabric.jar
- Updated Resourceful Lib: resourcefullib-fabric-1.20.1-2.1.25.jar -> resourcefullib-fabric-1.20.1-2.1.28.jar
- Updated Small Ships: smallships-fabric-1.20.1-2.0.0-b1.3.1.jar -> smallships-fabric-1.20.1-2.0.0-b1.4.jar
- Updated Stack Refill: stackrefill-1.20.1-4.4.jar -> stackrefill-1.20.1-4.5.jar
- Updated Traveler's Backpack: travelersbackpack-fabric-1.20.1-9.1.15.jar -> travelersbackpack-fabric-1.20.1-9.1.16.jar
- Migrate Litematica, MiniHUD & Tweakeroo to use Modrinth versions
**Server only**
- Added Eco Stack Manager to help prevent past situations we have had with lags

## 2.5.13
- **Added Create Contraption Terminals** (createcontraptionterminals-fabric-1.20-1.0.0.jar)
- Updated Botarium: botarium-fabric-1.20.1-2.3.3.jar -> botarium-fabric-1.20.1-2.3.4.jar
- Updated ChoiceTheorem's Overhauled Village: [Fabric]CTOV-v.3.4.4.jar -> [Fabric]CTOV-v3.4.6.jar
- Updated Collective: collective-1.20.1-7.64.jar -> collective-1.20.1-7.71.jar
- Updated Concurrent Chunk Management Engine (Fabric): c2me-fabric-mc1.20.1-0.2.0+alpha.11.5.jar -> c2me-fabric-mc1.20.1-0.2.0+alpha.11.7.jar
- Updated Create Railways Navigator: createrailwaysnavigator-0.5.3-beta-1.20.1-fabric.jar -> createrailwaysnavigator-fabric-1.20.1-0.5.5.jar
- Updated Create Slice & Dice: sliceanddice-fabric-3.2.2.jar -> sliceanddice-fabric-3.2.3.jar
- Updated Create: Copycats+: Copycats-fabric.1.20.1-1.3.4.jar -> copycats-2.0.5+mc.1.20.1-fabric.jar
- Updated Create: Metalwork: create-metalwork-1.0.3-fabric.jar -> create-metalwork-1.20.1-1.0.6-fabric.jar
- Updated CreateTweaker: CreateTweaker-fabric-1.20.1-4.0.9.jar -> CreateTweaker-fabric-1.20.1-4.0.10.jar
- Updated Customizable Player Models: CustomPlayerModels-Fabric-1.20-0.6.17a.jar -> CustomPlayerModels-Fabric-1.20-0.6.17b.jar
- Updated EMI: emi-1.1.7+1.20.1+fabric.jar -> emi-1.1.10+1.20.1+fabric.jar
- Updated Forgotten Graves: forgottengraves-3.2.10+1.20.1.jar -> forgottengraves-3.2.11+1.20.1.jar
- Updated Indium: indium-1.0.32+mc1.20.1.jar -> indium-1.0.34+mc1.20.1.jar
- Updated Jade 🔍: Jade-1.20-fabric-11.9.2.jar -> Jade-1.20-fabric-11.10.0.jar
- Updated Jade Addons (Fabric): JadeAddons-1.20.1-fabric-5.2.5.jar -> JadeAddons-1.20.1-fabric-5.2.6.jar
- Updated Just Enough Items: jei-1.20.1-fabric-15.3.0.8.jar -> jei-1.20.1-fabric-15.8.0.16.jar
- Updated ModernFix: modernfix-fabric-5.18.1+mc1.20.1.jar -> modernfix-fabric-5.18.10+mc1.20.1.jar
- Updated Moonlight Lib: moonlight-1.20-2.12.4-fabric.jar -> moonlight-1.20-2.12.8-fabric.jar
- Updated Puzzles Lib: PuzzlesLib-v8.1.20-1.20.1-Fabric.jar -> PuzzlesLib-v8.1.21-1.20.1-Fabric.jar
- Updated Remote Player Waypoints for Xaero's Map: remote_player_waypoints_for_xaero-fabric-2.1.0_1.20-1.20.1.jar -> remote_player_waypoints_for_xaero-fabric-3.1.0-1.20-1.20.1.jar
- Updated Sodium: sodium-fabric-0.5.10+mc1.20.1.jar -> sodium-fabric-0.5.11+mc1.20.1.jar
- Updated Terralith: Terralith_1.20_v2.5.1.jar -> Terralith_1.20.x_v2.5.4.jar
- Updated Tom's Simple Storage Mod: toms_storage_fabric-1.20-1.6.7.jar -> toms_storage_fabric-1.20-1.6.8.jar
- Updated Traveler's Backpack: travelersbackpack-fabric-1.20.1-9.1.13.jar -> travelersbackpack-fabric-1.20.1-9.1.15.jar


## 2.5.12
- Added RightClickHarvest Supplementaries Compat (rch-supplementaries-compat-fabric-1.0.0.jar)
- Updated Advanced Backups: AdvancedBackups-fabric-1.20-3.5.3.jar -> AdvancedBackups-fabric-1.20-3.6.jar
- Updated Amendments: amendments-1.20-1.2.4-fabric.jar -> amendments-1.20-1.2.8-fabric.jar
- Updated Balm: balm-fabric-1.20.1-7.3.4.jar -> balm-fabric-1.20.1-7.3.6.jar
- Updated Collective: collective-1.20.1-7.61.jar -> collective-1.20.1-7.64.jar
- Updated Create: Copycats+: Copycats-fabric.1.20.1-1.3.1.jar -> Copycats-fabric.1.20.1-1.3.4.jar
- Updated Create: Garnished: garnished-1.8.4+1.20.1-fabric.jar -> garnished-1.8.7+1.20.1-fabric.jar
- Updated Create: Metalwork: create-metalwork-1.0.2-fabric.jar -> create-metalwork-1.0.3-fabric.jar
- Updated Dynmap®: Dynmap-3.7-beta-4-fabric-1.20.jar -> Dynmap-3.7-beta-6-fabric-1.20.jar
- Updated EMI: emi-1.1.6+1.20.1+fabric.jar -> emi-1.1.7+1.20.1+fabric.jar
- Updated Geckolib: geckolib-fabric-1.20.1-4.4.6.jar -> geckolib-fabric-1.20.1-4.4.7.jar
- Updated Indium: indium-1.0.31+mc1.20.4.jar -> indium-1.0.32+mc1.20.1.jar
- Updated Just Enough Items: jei-1.20.1-fabric-15.3.0.7.jar -> jei-1.20.1-fabric-15.3.0.8.jar
- Updated Macaw's Doors: mcw-doors-1.1.0fabric-mc1.20.1.jar -> mcw-doors-1.1.1fabric-mc1.20.1.jar
- Updated Macaw's Fences and Walls: mcw-fences-1.1.1-mc1.20.1fabric.jar -> mcw-fences-1.1.2-mc1.20.1fabric.jar
- Updated Moonlight Lib: moonlight-1.20-2.11.41-fabric.jar -> moonlight-1.20-2.12.4-fabric.jar
- Updated Small Ships: smallships-fabric-1.20.1-2.0.0-b1.3.jar -> smallships-fabric-1.20.1-2.0.0-b1.3.1.jar
- Updated Sodium: sodium-fabric-0.5.8+mc1.20.1.jar -> sodium-fabric-0.5.10+mc1.20.1.jar
- Updated Supplementaries: supplementaries-1.20-2.8.15-fabric.jar -> supplementaries-1.20-2.8.17-fabric.jar

## 2.5.11
- Updated Advanced Backups: AdvancedBackups-fabric-1.20-3.5.2.jar -> AdvancedBackups-fabric-1.20-3.5.3.jar
- Updated ChoiceTheorem's Overhauled Village: ctov-fabric-3.4.3.jar -> [Fabric]CTOV-v.3.4.4.jar
- Updated Create Slice & Dice: sliceanddice-fabric-3.2.1.jar -> sliceanddice-fabric-3.2.2.jar
- Updated Create: Copycats+: Copycats-fabric.1.20.1-1.2.6.jar -> Copycats-fabric.1.20.1-1.3.1.jar
- Updated Create: Garnished: garnished-1.8.3+1.20.1-fabric.jar -> garnished-1.8.4+1.20.1-fabric.jar
- Updated Forgotten Graves: forgottengraves-3.2.9+1.20.1.jar -> forgottengraves-3.2.10+1.20.1.jar
- Updated ImmediatelyFast: ImmediatelyFast-Fabric-1.2.17+1.20.4.jar -> ImmediatelyFast-Fabric-1.2.18+1.20.4.jar
- Updated Just Enough Items: jei-1.20.1-fabric-15.3.0.6.jar -> jei-1.20.1-fabric-15.3.0.7.jar
- Updated Lithostitched: lithostitched-fabric-1.20.1-1.1.5.jar -> lithostitched-fabric-1.20.1-1.1.6.jar
- Updated ModernFix: modernfix-fabric-5.18.0+mc1.20.1.jar -> modernfix-fabric-5.18.1+mc1.20.1.jar
- Updated Moonlight Lib: moonlight-1.20-2.11.33-fabric.jar -> moonlight-1.20-2.11.41-fabric.jar
- Updated Tom's Simple Storage Mod: toms_storage_fabric-1.20-1.6.6.jar -> toms_storage_fabric-1.20-1.6.7.jar
- Updated YetAnotherConfigLib: YetAnotherConfigLib-3.4.4+1.20.1-fabric.jar -> YetAnotherConfigLib-3.5.0+1.20.1-fabric.jar

## 2.5.10
- **Removed Utility Belt mod** due to mod author removing all pre 2.0.0 persions (MC 1.20.4 and older) from modrinth
- Updated Amendments: amendments-1.20-1.1.31-fabric.jar -> amendments-1.20-1.2.4-fabric.jar
- Updated Balm: balm-fabric-1.20.1-7.2.2.jar -> balm-fabric-1.20.1-7.3.4.jar
- Updated Create: Garnished: garnished-1.8.1+1.20.1-fabric.jar -> garnished-1.8.3+1.20.1-fabric.jar
- Updated Create: Train Perspective: create-train-perspective-0.4.0.jar -> create-train-perspective-0.5.0+mc1.20.1.jar
- Updated Customizable Player Models: CustomPlayerModels-Fabric-1.20-0.6.16c.jar -> CustomPlayerModels-Fabric-1.20-0.6.17a.jar
- Updated Forgotten Graves: forgottengraves-3.2.8+1.20.1.jar -> forgottengraves-3.2.9+1.20.1.jar
- Updated Geckolib: geckolib-fabric-1.20.1-4.4.5.jar -> geckolib-fabric-1.20.1-4.4.6.jar
- Updated Indium: indium-1.0.30+mc1.20.4.jar -> indium-1.0.31+mc1.20.4.jar
- Updated Infinity Buttons: infinitybuttons-4.0.4-mc1.20.1.jar -> infinitybuttons-4.0.5-mc1.20.1.jar
- Updated Inventory Essentials: inventoryessentials-fabric-1.20.1-8.2.3.jar -> inventoryessentials-fabric-1.20.1-8.2.6.jar
- Updated Just Enough Items: jei-1.20.1-fabric-15.3.0.4.jar -> jei-1.20.1-fabric-15.3.0.6.jar
- Updated ModernFix: modernfix-fabric-5.17.0+mc1.20.1.jar -> modernfix-fabric-5.18.0+mc1.20.1.jar
- Updated Moonlight Lib: moonlight-1.20-2.11.30-fabric.jar -> moonlight-1.20-2.11.33-fabric.jar
- Updated Small Ships: smallships-fabric-1.20.1-2.0.0-b1.2.jar -> smallships-fabric-1.20.1-2.0.0-b1.3.jar
- Updated Supplementaries: supplementaries-1.20-2.8.10-fabric.jar -> supplementaries-1.20-2.8.15-fabric.jar
- Updated Tectonic: tectonic-fabric-1.20-2.3.4.jar -> tectonic-fabric-1.20-2.3.5b.jar
- Updated Xaero's Minimap: Xaeros_Minimap_24.1.1_Fabric_1.20.jar -> Xaeros_Minimap_24.2.0_Fabric_1.20.jar
- Updated Xaero's World Map: XaerosWorldMap_1.38.4_Fabric_1.20.jar -> XaerosWorldMap_1.38.8_Fabric_1.20.jar

## 2.5.9
Removed Modern Keybinding and mods relying on it as it seemed to be causing issues

- Removed Modern Keybinding
- Removed Sophisticated Core (Unofficial Fabric port)
- Removed Sophisticated Backpacks (Unofficial Fabric port)
- Updated Forgotten Graves: forgottengraves-1.20.1-3.2.6.jar -> forgottengraves-3.2.8+1.20.1.jar

## 2.5.8
- Re-added Noisium" (noisium-fabric-2.0.3+mc1.20-1.20.1.jar)
- Re-added Sophisticated Core (Unofficial Fabric port) (sophisticatedcore-0.5.109+mc1.20.1-SNAPSHOT-build.76.jar)
- Re-added Sophisticated Backpacks (Unoffical Fabric port) (sophisticatedbackpacks-3.19.5+mc1.20.1-SNAPSHOT-build.76.jar)

## 2.5.7
> Revert latest additions in an attempt to get the server working

- Remove Sophisticated Core (Unofficial Fabric port)
- Remove Sophisticated Backpacks (Unoffical Fabric port)

## 2.5.6-s1
### Server only
- Remove Noisium (to try to see what lags our server to death)

## 2.5.6
- Updated Create: Garnished: garnished-1.7.5+1.20.1.jar -> garnished-1.8.1+1.20.1-fabric.jar
- Updated Geckolib: geckolib-fabric-1.20.1-4.4.4.jar -> geckolib-fabric-1.20.1-4.4.5.jar
- Updated ImmediatelyFast: ImmediatelyFast-Fabric-1.2.15+1.20.4.jar -> ImmediatelyFast-Fabric-1.2.17+1.20.4.jar

## 2.5.5
- Downgrade Create: Garnished (garnished-1.7.5+1.20.1.jar) due to server launch problems https://github.com/DakotaPride/create-garnished/issues/65

## 2.5.4
- **Added Remote Player Waypoints for Xaero's Map (remote_player_waypoints_for_xaero-fabric-2.1.0_1.20-1.20.1.jar)**
- **Removed ThreatenGL** as it didn't seem to improve performance, modrinth actually took it down and modrinth team say it's not neccesary. Additinally supposedly it's causing crashes especially on older systems. Additionally Sodium already includes some of it's logic.
- Updated CC: Tweaked: cc-tweaked-1.20.1-fabric-1.110.3.jar -> cc-tweaked-1.20.1-fabric-1.111.0.jar
- Updated Collective: collective-1.20.1-7.60.jar -> collective-1.20.1-7.61.jar
- Updated Create: Garnished: garnished-1.7.5+1.20.1.jar -> garnished-1.8+1.20.1-fabric.jar
- Updated Fabric API: fabric-api-0.92.1+1.20.1.jar -> fabric-api-0.92.2+1.20.1.jar
- Updated YetAnotherConfigLib: YetAnotherConfigLib-3.4.2+1.20.1-fabric.jar -> YetAnotherConfigLib-3.4.4+1.20.1-fabric.jar

## 2.5.3-s1
- Fix Modern KeyBinding needing to exist on serverside also for Sophisticated Backpacks

## 2.5.3
You can now define a keybinding's activation key with key modifiers. Like "Ctrl + G" or "Alt + S";
The keybindings with the same activation key but different key conflict contexts no longer conflict.

This is all thanks to Modern Keybinding mod.

- Added Sophisticated Core (Unofficial Fabric port) (sophisticatedcore-0.5.109+mc1.20.1-SNAPSHOT-build.76.jar)
- Added Modern KeyBinding (ModernKeyBinding-Fabric-1.20-1.2.0.jar)
- Added Sophisticated Backpacks (Unoffical Fabric port) (sophisticatedbackpacks-3.19.5+mc1.20.1-SNAPSHOT-build.76.jar)
- Added Desired Servers (desiredservers-fabric-1.20.1-1.2.1.jar)
- Added Noisium (noisium-fabric-2.0.3+mc1.20-1.20.1.jar)
- Updated CraftTweaker: CraftTweaker-fabric-1.20.1-14.0.38.jar -> CraftTweaker-fabric-1.20.1-14.0.40.jar
- Updated Create: Power Loader (Fabric): create_power_loader-1.4.3-mc1.20.1-fabric.jar -> create_power_loader-1.5.3-mc1.20.1-fabric.jar
- Updated Moonlight Lib: moonlight-1.20-2.11.28-fabric.jar -> moonlight-1.20-2.11.30-fabric.jar
- Updated Polymorph: polymorph-fabric-0.49.3+1.20.1.jar -> polymorph-fabric-0.49.5+1.20.1.jar
- Updated Puzzles Lib: PuzzlesLib-v8.1.19-1.20.1-Fabric.jar -> PuzzlesLib-v8.1.20-1.20.1-Fabric.jar
- Updated Traveler's Backpack: travelersbackpack-fabric-1.20.1-9.1.12.jar -> travelersbackpack-fabric-1.20.1-9.1.13.jar
- Updated Where Is It: whereisit-2.4.1+1.20.1.jar -> whereisit-2.4.3+1.20.1.jar

## 2.5.2
- Updated Amendments: amendments-1.20-1.1.30-fabric.jar -> amendments-1.20-1.1.31-fabric.jar
- Updated Collective: collective-1.20.1-7.57.jar -> collective-1.20.1-7.60.jar
- Updated Create Railways Navigator: createrailwaysnavigator-0.5.2-beta-1.20.1-fabric.jar -> createrailwaysnavigator-0.5.3-beta-1.20.1-fabric.jar
- Updated Create: Metalwork: create-metalwork-1.0.1-fabric.jar -> create-metalwork-1.0.2-fabric.jar
- Updated Create: Train Perspective: create-train-perspective-0.3.0.jar -> create-train-perspective-0.4.0.jar
- Updated Fabric Language Kotlin: fabric-language-kotlin-1.10.20+kotlin.1.9.24.jar -> fabric-language-kotlin-1.11.0+kotlin.2.0.0.jar
- Updated ImmediatelyFast: ImmediatelyFast-Fabric-1.2.14+1.20.4.jar -> ImmediatelyFast-Fabric-1.2.15+1.20.4.jar
- Updated Immersive Aircraft: immersive_aircraft-1.0.0+1.20.1-fabric.jar -> immersive_aircraft-1.0.1+1.20.1-fabric.jar
- Updated Moonlight Lib: moonlight-1.20-2.11.22-fabric.jar -> moonlight-1.20-2.11.28-fabric.jar
- Updated Resourceful Lib: resourcefullib-fabric-1.20.1-2.1.24.jar -> resourcefullib-fabric-1.20.1-2.1.25.jar
- Updated Stack Refill: stackrefill-1.20.1-4.2.jar -> stackrefill-1.20.1-4.4.jar
- Updated ThreatenGL: threatengl-fabric-1.2.1-beta.3.jar -> threatengl-fabric-1.3.2-beta.1.jar

## 2.5.1
- Rolled back Create: Metalwork to create-metalwork-1.0.1-fabric.jar

## 2.5.0
Added [ThreatenGL](https://modrinth.com/mod/threatengl) mod as testing (tested locally with 2 different GPUs, in both cases it did seem to improve my game perfomance, even with Iris & Iris/Flyweel installed.). I saw the game actually using my GPU after adding this.

- **Updated Fabric loader to version 0.15.11**
- **Added ThreatenGL" (threatengl-fabric-1.2.1-beta.3.jar)**
- Updated Amendments: amendments-1.20-1.1.27-fabric.jar -> amendments-1.20-1.1.30-fabric.jar        
- Updated Create: Metalwork: create-metalwork-1.0.1-fabric.jar -> create-metalwork-1.0.2-fabric.jar 
- Updated Moonlight Lib: moonlight-1.20-2.11.17-fabric.jar -> moonlight-1.20-2.11.22-fabric.jar     
- Updated Terralith: Terralith_1.20_v2.5.0.jar -> Terralith_1.20_v2.5.1.jar
- Updated Where Is It: whereisit-2.4.0+1.20.1.jar -> whereisit-2.4.1+1.20.1.jar
Create: Train Perspective: create-train-perspective-0.2.3.jar -> create-train-perspective-0.3.0.- Updated jar

## 2.4.13
Added Create themed chunk loaders which we'll be experimenting with instead of the current mod

- **Added Create: Power Loader (Fabric) (create_power_loader-1.4.3-mc1.20.1-fabric.jar)**
- Updated Create Railways Navigator: createrailwaysnavigator-0.5.1-beta-1.20.1.jar -> createrailwaysnavigator-0.5.2-beta-1.20.1-fabric.jar
- Updated Create: Diesel Generators [Fabric]: creatediselgenerators-1.2.3h.jar -> createdieselgenerators-2.1.4.jar
- Updated Create: Steam 'n' Rails: Steam_Rails-1.6.3+fabric-mc1.20.1.jar -> Steam_Rails-1.6.4+fabric-mc1.20.1.jar
- Updated Create: Train Perspective: create-train-perspective-0.2.2.jar -> create-train-perspective-0.2.3.jar
- Updated Infinity Buttons: infinitybuttons-4.0.1-mc1.20.1.jar -> infinitybuttons-4.0.4-mc1.20.1.jar
- Updated Jade 🔍: Jade-1.20-fabric-11.9.0.jar -> Jade-1.20-fabric-11.9.2.jar
- Updated Puzzles Lib: PuzzlesLib-v8.1.18-1.20.1-Fabric.jar -> PuzzlesLib-v8.1.19-1.20.1-Fabric.jar
- Updated YUNG's API: YungsApi-1.20-Fabric-4.0.4.jar -> YungsApi-1.20-Fabric-4.0.5.jar
- Updated YUNG's Better Jungle Temples: YungsBetterJungleTemples-1.20-Fabric-2.0.4.jar -> YungsBetterJungleTemples-1.20-Fabric-2.0.5.jar

## 2.4.12
- Downgraded Jade to Jade-1.20-fabric-11.9.0.jar (https://github.com/Snownee/Jade/issues/402)

## 2.4.11
- **Added Create Railways Navigator (createrailwaysnavigator-0.5.1-beta-1.20.1.jar)**
- **Added Infinity Buttons (infinitybuttons-4.0.1-mc1.20.1.jar)**
- Updated Advanced Backups: AdvancedBackups-fabric-1.20-3.5.1.jar -> AdvancedBackups-fabric-1.20-3.5.2.jar
- Updated Amendments: amendments-1.20-1.1.27-fabric.jar -> amendments-1.20-1.1.27-fabric.jar
- Updated Collective: collective-1.20.1-7.56.jar -> collective-1.20.1-7.57.jar
- Updated Fabric Language Kotlin: fabric-language-kotlin-1.10.19+kotlin.1.9.23.jar -> fabric-language-kotlin-1.10.20+kotlin.1.9.24.jar
- Updated Jade 🔍: Jade-1.20-fabric-11.9.0.jar -> Jade-1.20-fabric-11.9.1.jar
- Updated Small Ships: smallships-fabric-1.20.1-2.0.0-b1.1.jar -> smallships-fabric-1.20.1-2.0.0-b1.2.jar
- Updated Terralith: Terralith_1.20.4_v2.4.11.jar -> Terralith_1.20_v2.5.0.jar

## 2.4.10-s1
- Some EMI mods being clientside only but server does need them

## 2.4.10
> Remove REI (Roughly Enough Items) and it's addons and replaced it with newer and more powerful [EMI](https://modrinth.com/mod/emi)

- **Removed Roughly Enough Items (REI)**
- **Removed Roughly Enough Resources**
- **Project "EMI" successfully added! (emi-1.1.6+1.20.1+fabric.jar)**
- **Project "EMI Loot" successfully added! (emi_loot-0.6.6+1.20.1.jar)**
- **Project "EMI Ores" successfully added! (emi_ores-0.4+1.20.1.jar)**
- **Project "EMI Trades" successfully added! (emitrades-fabric-1.2.1+mc1.20.1.jar)**
- **Project "Just Enough Items" successfully added! (jei-1.20.1-fabric-15.3.0.4.jar)** (improves EMI compatiblity if installed, automatically disabled)
- **Added Create: Interiors (interiors-0.5.3+fabric-mc1.20.1.jar)**
- Updated ImmediatelyFast: ImmediatelyFast-Fabric-1.2.13+1.20.4.jar -> ImmediatelyFast-Fabric-1.2.14+1.20.4.jar
### Server
- Removed Fabric TPS (didn't help me as I hoped)
- Removed Discord's `/tps` command as it didn't work

## 2.4.9
- **Fixes the charcoal > coal receipe**

## 2.4.8
- **[Closes #38](https://gitlab.com/thesociety/transportcraft-lite/-/issues/38) Added receipe to craft charcoal to coal**
- Updated Amendments: amendments-1.20-1.1.26-fabric.jar -> amendments-1.20-1.1.27-fabric.jar
- Updated Create: Design n' Decor: design_decor-0.2a+1.20.1-12.jar -> design_decor-0.4_fabric+1.20.1.jar
- Updated Moonlight Lib: moonlight-1.20-2.11.14-fabric.jar -> moonlight-1.20-2.11.15-fabric.jar
- Updated NiftyCarts: niftycarts-3.0.1+1.20.1.jar -> niftycarts-3.0.2+1.20.1.jar

## 2.4.7
- **Added proper new receipe for the Phantom Track**
- Added CreateTweaker (CreateTweaker-fabric-1.20.1-4.0.9.jar)
- Tried to fix Discord `/tps` command for server

## 2.4.6
- **Added alternative crafting receipe for Phantom Rails**
- **Re-added Where Is It (whereisit-2.4.0+1.20.1.jar)**
> To use it, find a item in REI, hover over it with mouse and press the key you have binded as `Search for Item` (default Y). It will highlight any storage in the world that has that item within 50 block radious.
- Removed Chest Tracker (replaced with Where Is It, this one had too big limitation)
- **Removed Simple Trashcan (it didn't work well)**
- Re-added Amendments (amendments-1.20-1.1.26-fabric.jar)
- Updated Collective: collective-1.20.1-7.54.jar -> collective-1.20.1-7.56.jar
- Updated ImmediatelyFast: ImmediatelyFast-Fabric-1.2.12+1.20.4.jar -> ImmediatelyFast-Fabric-1.2.13+1.20.4.jar
- Updated Moonlight Lib: moonlight-1.20-2.11.12-fabric.jar -> moonlight-1.20-2.11.14-fabric.jar

**Server**
- Added Fabric TPS (fabric_tps-1.20.1-1.4.0.jar)

## 2.4.5
- **Removed amendments**
- **Project "Moonlight Lib" successfully added! (moonlight-1.20-2.11.12-fabric.jar)

## 2.4.4
- **Change Transportcraft Official server IP** If this is not your first time launching the pack, you need to edit the entry to new IP: transportcraft.thesociety.eu
- **Remove CreateFabric&REIBugFix** - no longer needed and actually broken
- amendments: amendments-1.20-1.1.22-fabric.jar -> amendments-1.20-1.1.22-fabric.jar
- Moonlight Lib: moonlight-1.20-2.11.12-fabric.jar -> moonlight-1.20-2.11.13-fabric.jar
- Roughly Enough Items (REI): RoughlyEnoughItems-12.1.718-fabric.jar -> RoughlyEnoughItems-12.1.725-fabric.jar
- YetAnotherConfigLib: YetAnotherConfigLib-3.4.1+1.20.1-fabric.jar -> YetAnotherConfigLib-3.4.2+1.20.1-fabric.jar

## 2.4.3
- Remove BCLib (was dependancy for BetterEnd which we checked out)

## 2.4.2
- **[Society Server Only] Chipped blocks can now be created with Create: Mechanical saw** _If you want this feature in your own game, you need this datapack: https://modrinth.com/datapack/create-chipped-cutting_
---
- Removed William Wythers' Overhauled Overworld
- **Project "Terralith" successfully added! (Terralith_1.20.4_v2.4.11.jar)**
- **Project "Tectonic" successfully added! (tectonic-fabric-1.20-2.3.4.jar)**
- Dependency "BCLib" successfully added! (bclib-3.0.14.jar)
---
- Project "Lithostitched" successfully added! (lithostitched-fabric-1.20.1-1.1.5.jar)
- Removed CTOV - Create: Structures _Lithostitched handles this now_
- Removed CTOV - Farmer Delight Compat _Lithostitched handles this now_
---
- **Removed Create Crafts & Additions** _Fabric port is not actively maintained, Fabric version is missing cool stuff and we've encountered issues with it_
- Dependency "Collective" successfully added! (collective-1.20.1-7.54.jar)
- **Project "Stack Refill" successfully added! (stackrefill-1.20.1-4.2.jar)**
- **Project "Create: Big Contraptions" successfully added! (bigcontraptions-1.0.jar)**
- **Project "Tom's Simple Storage Mod" successfully added! (toms_storage_fabric-1.20-1.6.6.jar)**
- **Project "Create: Diesel Generators [Fabric]" successfully added! (creatediselgenerators-1.2.3h.jar)**
- Customizable Player Models: CustomPlayerModels-Fabric-1.20-0.6.16b.jar -> CustomPlayerModels-Fabric-1.20-0.6.16c.jar
- Farmer's Delight Refabricated: FarmersDelight-1.20.1-2.1.0+refabricated.jar -> FarmersDelight-1.20.1-2.1.1+refabricated.jar
- Scarecrows' Territory: scarecrowsterritory-1.1.11-fabric-mc1.20.jar -> scarecrowsterritory-1.1.11a-fabric-mc1.20.jar

## 2.3.1
- Updated CC: Tweaked: cc-tweaked-1.20.1-fabric-1.110.2.jar -> cc-tweaked-1.20.1-fabric-1.110.3.jar
- Updated Create Deco: createdeco-2.0.1-1.20.1-fabric.jar -> createdeco-2.0.2-1.20.1-fabric.jar
- Updated Create: Metalwork: create-metalwork-1.0.0-fabric.jar -> create-metalwork-1.0.1-fabric.jar
- Updated Create: Train Perspective: create-train-perspective-0.2.1.jar -> create-train-perspective-0.2.2.jar
- Updated Farmer's Delight Refabricated: FarmersDelight-1.20.1-2.0.17+refabricated.jar -> FarmersDelight-1.20.1-2.1.0+refabricated.jar
- Updated ImmediatelyFast: ImmediatelyFast-Fabric-1.2.11+1.20.4.jar -> ImmediatelyFast-Fabric-1.2.12+1.20.4.jar
- Updated Jade 🔍: Jade-1.20-fabric-11.8.0.jar -> Jade-1.20-fabric-11.9.0.jar
- Updated Jade Addons (Fabric): JadeAddons-1.20.1-fabric-5.2.3.jar -> JadeAddons-1.20.1-fabric-5.2.5.jar
- Updated ModernFix: modernfix-fabric-5.16.1+mc1.20.1.jar -> modernfix-fabric-5.17.0+mc1.20.1.jar
- Updated Roughly Enough Items (REI): RoughlyEnoughItems-12.1.712-fabric.jar -> RoughlyEnoughItems-12.1.718-fabric.jar
- Updated Searchables: Searchables-fabric-1.20.1-1.0.2.jar -> Searchables-fabric-1.20.1-1.0.3.jar
- Updated YetAnotherConfigLib: yet-another-config-lib-fabric-3.2.2+1.20.jar -> YetAnotherConfigLib-3.4.1+1.20.1-fabric.jar

## 2.3.0
- **Updated Fabric loader to version 0.15.10**
- **Removed [deprecated Farmer's Delight](https://modrinth.com/mod/farmers-delight-fabric) mod**
- **Added Farmer's Delight Refabricated (FarmersDelight-1.20.1-2.0.17+refabricated.jar)** 
- **Added Create: Design n' Decor (design_decor-0.2a+1.20.1-12.jar)**
- **Added Ruined Equipment (ruined-equipment-2.3.1+1.20.1.jar)**
- **Added Simple Trashcan (SimpleTrashcan-fabric-1.20.1-1.0.0.jar)**
- **Added Create: Metalwork (create-metalwork-1.0.0-fabric.jar)**
- **Added Create: Train Perspective (create-train-perspective-0.2.1.jar)**
- Updated Advanced Backups: AdvancedBackups-fabric-1.20-3.4.jar -> AdvancedBackups-fabric-1.20-3.5.1.jar
- Updated amendments: amendments-1.20-1.1.11-fabric.jar -> amendments-1.20-1.1.22-fabric.jar
- Updated CC: Tweaked: cc-tweaked-1.20.1-fabric-1.110.1.jar -> cc-tweaked-1.20.1-fabric-1.110.2.jar
- Updated Chipped: Chipped-fabric-1.20.1-3.0.4.jar -> Chipped-fabric-1.20.1-3.0.6.jar
- Updated ChoiceTheorem's Overhauled Village: ctov-3.4.2.jar -> ctov-fabric-3.4.3.jar
- Updated Clumps: Clumps-fabric-1.20.1-12.0.0.3.jar -> Clumps-fabric-1.20.1-12.0.0.4.jar
- Updated CraftTweaker: CraftTweaker-fabric-1.20.1-14.0.36.jar -> CraftTweaker-fabric-1.20.1-14.0.38.jar 
- Updated Create Fabric: create-fabric-0.5.1-f-build.1335+mc1.20.1.jar -> create-fabric-0.5.1-f-build.1417+mc1.20.1.jar
- Updated Create Slice & Dice: sliceanddice-fabric-3.1.0.jar -> sliceanddice-fabric-3.2.1.jar
- Updated Create: Copycats+: Copycats-fabric.1.20.1-1.2.2.jar -> Copycats-fabric.1.20.1-1.2.6.jar
- Updated Create: Steam 'n' Rails: Steam_Rails-1.5.3+fabric-mc1.20.1.jar -> Steam_Rails-1.6.3+fabric-mc1.20.1.jar
- Updated Customizable Player Models: CustomPlayerModels-Fabric-1.20-0.6.15a.jar -> CustomPlayerModels-Fabric-1.20-0.6.16b.jar
- Updated Discord Integration: dcintegration-fabric-3.0.6-1.20.1.jar -> dcintegration-fabric-3.0.7-1.20.1.jar
- Updated Fabric API: fabric-api-0.92.0+1.20.1.jar -> fabric-api-0.92.1+1.20.1.jar
- Updated Forgotten Graves: forgottengraves-1.20.1-3.2.5.jar -> forgottengraves-1.20.1-3.2.6.jar
- Updated ImmediatelyFast: ImmediatelyFast-Fabric-1.2.10+1.20.4.jar -> ImmediatelyFast-Fabric-1.2.11+1.20.4.jar
- Updated Macaw's Bridges: mcw-bridges-2.1.0-mc1.20.1fabric.jar -> mcw-bridges-3.0.0-mc1.20.1fabric.jar
- Updated ModernFix: modernfix-fabric-5.15.0+mc1.20.1.jar -> modernfix-fabric-5.16.1+mc1.20.1.jar
- Updated Moonlight Lib: moonlight-1.20-2.11.9-fabric.jar -> moonlight-1.20-2.11.12-fabric.jar
- Updated More Culling: moreculling-1.20.4-0.22.1.jar -> moreculling-1.20.4-0.24.0.jar
- Updated Mouse Tweaks: MouseTweaks-fabric-mc1.20-2.25.jar -> MouseTweaks-fabric-mc1.20-2.26.jar
- Updated Polymorph: polymorph-fabric-0.49.1+1.20.1.jar -> polymorph-fabric-0.49.3+1.20.1.jar
- Updated Resourceful Lib: resourcefullib-fabric-1.20.1-2.1.23.jar -> resourcefullib-fabric-1.20.1-2.1.24.jar
- Updated Roughly Enough Items (REI): RoughlyEnoughItems-12.0.684-fabric.jar -> RoughlyEnoughItems-12.1.712-fabric.jar
- Updated Supplementaries: supplementaries-1.20-2.8.4-fabric.jar -> supplementaries-1.20-2.8.10-fabric.jar
- Updated Traveler's Backpack: travelersbackpack-fabric-1.20.1-9.1.10.jar -> travelersbackpack-fabric-1.20.1-9.1.11.jar
- Updated Traveler's Backpack: travelersbackpack-fabric-1.20.1-9.1.11.jar -> travelersbackpack-fabric-1.20.1-9.1.12.jar
- Updated Xaero's Minimap: Xaeros_Minimap_24.0.2_Fabric_1.20.jar -> Xaeros_Minimap_24.1.1_Fabric_1.20.jar
- Updated Xaero's World Map: XaerosWorldMap_1.38.1_Fabric_1.20.jar -> XaerosWorldMap_1.38.4_Fabric_1.20.jar
- Updated YUNG's Better Dungeons: YungsBetterDungeons-1.20-Fabric-4.0.3.jar -> YungsBetterDungeons-1.20-Fabric-4.0.4.jar

## 2.2.0
> Create: Fabric migrated to latest main version. Open Simple Frames replaced with Create: Copycats+

- **Updated Fabric loader to version 0.15.7**
- **Removed Create: Fabric (Sodium Fix)** - added the updated main version
- **Removed Create Fabric Fixes** - all issues supposedly fixed in latest Create: Fabric
- **Added "Create Fabric" (create-fabric-0.5.1-f-build.1335+mc1.20.1.jar)**
- **Removed Open Simple Frames** - too many issues, alternative Create: Copycats+ added
- **Added Create: Copycats+ (Copycats-fabric.1.20.1-1.2.2.jar)** to replce the broken Open Simple Frames
- Removed Create related fabric mod check overrrides
- **Added "amendments" (amendments-1.20-1.1.10-fabric.jar)** - split mod from Supplementaries
- Updated amendments: amendments-1.20-1.1.10-fabric.jar -> amendments-1.20-1.1.11-fabric.jar
- Updated Architectury API: architectury-9.1.13-fabric.jar -> architectury-9.2.14-fabric.jar
- Updated Athena: athena-fabric-1.20.1-3.1.1.jar -> athena-fabric-1.20.1-3.1.2.jar
- Updated Balm: balm-fabric-1.20.1-7.2.1.jar -> balm-fabric-1.20.1-7.2.2.jar
- Updated Botarium: botarium-fabric-1.20.1-2.3.2.jar -> botarium-fabric-1.20.1-2.3.3.jar
- Updated Building Wands: BuildingWands-mc1.20.1-2.6.8-release-fabric.jar -> BuildingWands-mc1.20.1-2.6.9-release.jar
- Updated CC: Tweaked: cc-tweaked-1.20.1-fabric-1.109.5.jar -> cc-tweaked-1.20.1-fabric-1.110.1.jar
- Updated CraftTweaker: CraftTweaker-fabric-1.20.1-14.0.33.jar -> CraftTweaker-fabric-1.20.1-14.0.36.jar
- Updated Create Crafts & Additions: createaddition-fabric+1.20.1-0.9.0.jar -> createaddition-fabric+1.20.1-1.2.3.jar
- Updated Create Enchantment Industry Fabric: create_enchantment_industry-1.2.12.jar -> create_enchantment_industry-1.2.16.jar
- Updated Create Jetpack: create_jetpack-fabric-4.1.0.jar -> create_jetpack-fabric-4.2.0.jar
- Updated Create Slice & Dice: sliceanddice-fabric-3.0.0.jar -> sliceanddice-fabric-3.1.0.jar
- Updated Create: Garnished: garnished-1.6.3+1.20.1.jar -> garnished-1.7.5+1.20.1.jar
- Updated Create: Steam 'n' Rails: Steam_Rails-1.5.1+fabric-mc1.20.1.jar -> Steam_Rails-1.5.3+fabric-mc1.20.1.jar
- Updated Customizable Player Models: CustomPlayerModels-Fabric-1.20-0.6.14c.jar -> CustomPlayerModels-Fabric-1.20-0.6.15a.jar
- Updated Discord Integration: dcintegration-fabric-3.0.5-1.20.1.jar -> dcintegration-fabric-3.0.6-1.20.1.jar
- Updated Fabric Language Kotlin: fabric-language-kotlin-1.10.17+kotlin.1.9.22.jar -> fabric-language-kotlin-1.10.19+kotlin.1.9.23.jar
- Updated Fusion (Connected Textures): fusion-1.1.0c-fabric-mc1.20.1.jar -> fusion-1.1.1-fabric-mc1.20.1.jar
- Updated Geckolib: geckolib-fabric-1.20.1-4.4.2.jar -> geckolib-fabric-1.20.1-4.4.4.jar
- Updated Inventory Essentials: inventoryessentials-fabric-1.20.1-8.2.1.jar -> inventoryessentials-fabric-1.20.1-8.2.3.jar
- Updated Macaw's Fences and Walls: mcw-fences-1.1.0-mc1.20.1fabric.jar -> mcw-fences-1.1.1-mc1.20.1fabric.jar
- Updated ModernFix: modernfix-fabric-5.13.0+mc1.20.1.jar -> modernfix-fabric-5.15.0+mc1.20.1.jar
- Updated Moonlight Lib: moonlight-1.20-2.11.2-fabric.jar -> moonlight-1.20-2.11.9-fabric.jar
- Updated Puzzles Lib: PuzzlesLib-v8.1.16-1.20.1-Fabric.jar -> PuzzlesLib-v8.1.18-1.20.1-Fabric.jar
- Updated Small Ships: smallships-fabric-1.20.1-2.0.0-b1.0.jar -> smallships-fabric-1.20.1-2.0.0-b1.1.jar
- Updated Supplementaries: supplementaries-1.20-2.7.36-fabric.jar -> supplementaries-1.20-2.8.4-fabric.jar
- Updated Traveler's Backpack: travelers-backpack-1.20.1-9.1.9.jar -> travelersbackpack-fabric-1.20.1-9.1.10.jar
- Updated Trinkets: trinkets-3.7.1.jar -> trinkets-3.7.2.jar
- Updated Xaero's Minimap: Xaeros_Minimap_23.9.7_Fabric_1.20.jar -> Xaeros_Minimap_24.0.2_Fabric_1.20.jar
- Updated Xaero's World Map: XaerosWorldMap_1.37.8_Fabric_1.20.jar -> XaerosWorldMap_1.38.1_Fabric_1.20.jar
- Updated YUNG's API: YungsApi-1.20-Fabric-4.0.3.jar -> YungsApi-1.20-Fabric-4.0.4.jar
- Updated YUNG's Better Nether Fortresses: YungsBetterNetherFortresses-1.20-Fabric-2.0.5.jar -> YungsBetterNetherFortresses-1.20-Fabric-2.0.6.jar

## 2.1.11
- Fixes version check throwing a html code instead of newest version
- Update Architectury API: architectury-9.1.12-fabric.jar -> architectury-9.1.13-fabric.jar
- Update Chipped: chipped-fabric-1.20.1-3.0.1.jar -> Chipped-fabric-1.20.1-3.0.4.jar
- Update ChoiceTheorem's Overhauled Village: ctov-3.3.6.jar -> ctov-3.4.2.jar
- Update Concurrent Chunk Management Engine (Fabric): c2me-fabric-mc1.20.1-0.2.0+alpha.11.0.jar -> c2me-fabric-mc1.20.1-0.2.0+alpha.11.5.jar   
- Update CraftTweaker: CraftTweaker-fabric-1.20.1-14.0.32.jar -> CraftTweaker-fabric-1.20.1-14.0.33.jar
- Update Create: Garnished: garnished-1.6.1+1.20.1.jar -> garnished-1.6.3+1.20.1.jar
- Update Create: New Age: create-new-age-fabric-1.20.1-1.1.1.jar -> create-new-age-fabric-1.20.1-1.1.2.jar
- Update Customizable Player Models: CustomPlayerModels-Fabric-1.20-0.6.14b.jar -> CustomPlayerModels-Fabric-1.20-0.6.14c.jar
- Update Fabric API: fabric-api-0.91.0+1.20.1.jar -> fabric-api-0.92.0+1.20.1.jar
- Update ImmediatelyFast: ImmediatelyFast-Fabric-1.2.8+1.20.4.jar -> ImmediatelyFast-Fabric-1.2.10+1.20.4.jar
- Update Indium: indium-1.0.27+mc1.20.1.jar -> indium-1.0.30+mc1.20.4.jar
- Update Jade 🔍: Jade-1.20-fabric-11.7.0.jar -> Jade-1.20-fabric-11.8.0.jar
- Update ModernFix: modernfix-fabric-5.12.1+mc1.20.1.jar -> modernfix-fabric-5.13.0+mc1.20.1.jar
- Update Moonlight Lib: moonlight-1.20-2.9.7-fabric.jar -> moonlight-1.20-2.9.16-fabric.jar
- Update NiftyCarts: niftycarts-3.0.0+1.20.1.jar -> niftycarts-3.0.1+1.20.1.jar
- Update Resourceful Lib: resourcefullib-fabric-1.20.1-2.1.21.jar -> resourcefullib-fabric-1.20.1-2.1.23.jar
- Update SuperMartijn642's Core Lib: supermartijn642corelib-1.1.16-fabric-mc1.20.1.jar -> supermartijn642corelib-1.1.17-fabric-mc1.20.1.jar    
- Update Supplementaries: supplementaries-1.20-2.7.32-fabric.jar -> supplementaries-1.20-2.7.36-fabric.jar
- Update YetAnotherConfigLib: yet-another-config-lib-fabric-3.2.1+1.20.jar -> yet-another-config-lib-fabric-3.2.2+1.20.jar
- Updated Sodium sodium-fabric-mc1.20.1-0.5.3.jar -> sodium-fabric-0.5.8+mc1.20.1.jar

## 2.1.10
- **Added Small Ships (smallships-fabric-1.20.1-2.0.0-b1.0.jar)**
- **Added Update ME! (UpdateMe-2.0.jar)**

## 2.1.9
- **Updated Fabric to version 0.15.6**
- **Added Advanced Backups (AdvancedBackups-fabric-1.20-3.4.jar)** - mod to handle server backups
- Update Botarium: botarium-fabric-1.20.1-2.3.1.jar -> botarium-fabric-1.20.1-2.3.2.jar
- Update CC: Tweaked: cc-tweaked-1.20.1-fabric-1.109.4.jar -> cc-tweaked-1.20.1-fabric-1.109.5.jar
- Update CraftTweaker: CraftTweaker-fabric-1.20.1-14.0.30.jar -> CraftTweaker-fabric-1.20.1-14.0.32.jar
- Update Create: Garnished: garnished-1.5.2-hotfix-1+1.20.1.jar -> garnished-1.6.1+1.20.1.jar
- Update Moonlight Lib: moonlight-1.20-2.9.6-fabric.jar -> moonlight-1.20-2.9.7-fabric.jar
- Update Open Simple Frames: OpenSimpleFrames-1.1.0.jar -> simpleframes-1.2.0.jar
- Update Puzzles Lib: PuzzlesLib-v8.1.15-1.20.1-Fabric.jar -> PuzzlesLib-v8.1.16-1.20.1-Fabric.jar
- Update Resourceful Lib: resourcefullib-fabric-1.20.1-2.1.20.jar -> resourcefullib-fabric-1.20.1-2.1.21.jar
- Update Supplementaries: supplementaries-1.20-2.7.31-fabric.jar -> supplementaries-1.20-2.7.32-fabric.jar
- Update Xaero's World Map: XaerosWorldMap_1.37.7_Fabric_1.20.jar -> XaerosWorldMap_1.37.8_Fabric_1.20.jar
- Update YUNG's API: YungsApi-1.20-Fabric-4.0.2.jar -> YungsApi-1.20-Fabric-4.0.3.jar

## 2.1.8
- Fixed incorrectly removed mod

## 2.1.7
- Updated CC: Tweaked: cc-tweaked-1.20.1-fabric-1.109.3.jar -> cc-tweaked-1.20.1-fabric-1.109.4.jar
- Updated Create Deco: createdeco-2.0.0-fabric-1.20.1.jar -> createdeco-2.0.1-1.20.1-fabric.jar
- Updated Create Fabric Fixes: createfabricfixes-0.0.2+1.20.1.jar -> createfabricfixes-0.0.3+1.20.1-create-0.5.1d.jar
- Updated Customizable Player Models: CustomPlayerModels-Fabric-1.20-0.6.14a.jar -> CustomPlayerModels-Fabric-1.20-0.6.14b.jar
- Updated Geckolib: geckolib-fabric-1.20.1-4.4.jar -> geckolib-fabric-1.20.1-4.4.2.jar
- Updated Moonlight Lib: moonlight-1.20-2.8.84-fabric.jar -> moonlight-1.20-2.9.6-fabric.jar
- Updated More Culling: moreculling-1.20.4-0.22.0.jar -> moreculling-1.20.4-0.22.1.jar
- Updated Puzzles Lib: PuzzlesLib-v8.1.13-1.20.1-Fabric.jar -> PuzzlesLib-v8.1.15-1.20.1-Fabric.jar
- Updated Supplementaries: supplementaries-1.20-2.7.25-fabric.jar -> supplementaries-1.20-2.7.31-fabric.jar
- Updated Xaero's Minimap: Xaeros_Minimap_23.9.3_Fabric_1.20.jar -> Xaeros_Minimap_23.9.7_Fabric_1.20.jar
- Updated Xaero's World Map: XaerosWorldMap_1.37.2_Fabric_1.20.jar -> XaerosWorldMap_1.37.7_Fabric_1.20.jar

## 2.1.6
- **Added Customizable Player Models (CustomPlayerModels-Fabric-1.20-0.6.14a.jar)**
- Updated Botarium: botarium-fabric-1.20.1-2.3.0.jar -> botarium-fabric-1.20.1-2.3.1.jar
- Updated Create Enchantment Industry Fabric: create_enchantment_industry-1.2.11.jar -> create_enchantment_industry-1.2.12.jar
- Updated Create: Garnished: garnished-1.5.1-hotfix-1+1.20.1.jar -> garnished-1.5.2-hotfix-1+1.20.1.jar
- Updated Enderman Tweaks: enderman_tweaks-0.3.0.jar -> enderman_tweaks-0.3.1.jar
- Updated Macaw's Fences and Walls: mcw-fences-1.0.7-mc1.20.1fabric.jar -> mcw-fences-1.1.0-mc1.20.1fabric.jar
- Updated Magnum Torch: MagnumTorch-v8.0.1-1.20.1-Fabric.jar -> MagnumTorch-v8.0.2-1.20.1-Fabric.jar
- Updated ModernFix: modernfix-fabric-5.11.1+mc1.20.1.jar -> modernfix-fabric-5.12.1+mc1.20.1.jar
- Updated Moonlight Lib: moonlight-1.20-2.8.81-fabric.jar -> moonlight-1.20-2.8.84-fabric.jar
- Updated More Culling: moreculling-1.20.4-0.21.0.jar -> moreculling-1.20.4-0.22.0.jar
- Updated Puzzles Lib: PuzzlesLib-v8.1.12-1.20.1-Fabric.jar -> PuzzlesLib-v8.1.13-1.20.1-Fabric.jar
- Updated Supplementaries: supplementaries-1.20-2.7.21-fabric.jar -> supplementaries-1.20-2.7.25-fabric.jar

## 2.1.5
- Updated Botarium: botarium-fabric-1.20.1-2.2.2.jar -> botarium-fabric-1.20.1-2.3.0.jar
- Updated Extended Drawers: ExtendedDrawers-2.1.0+mc.1.20.1.jar -> ExtendedDrawers-2.1.1+mc.1.20.1.jar
- Updated Geckolib: geckolib-fabric-1.20.1-4.3.1.jar -> geckolib-fabric-1.20.1-4.4.jar
- Updated Moonlight Lib: moonlight-1.20-2.8.80-fabric.jar -> moonlight-1.20-2.8.81-fabric.jar
- Updated More Culling: moreculling-1.20.4-0.20.3.jar -> moreculling-1.20.4-0.21.0.jar

## 2.1.4
- **Removed Valkyrien Skies**
- **Removed Eureka**
- Replace Create Fabric with the Create Fabric Sodium fix again (create-sodium-fix-0.5.1-d-build.5+mc1.20.1.jar)
- Re-added Sodium (sodium-fabric-mc1.20.1-0.5.3.jar)
- Re-added Indium (indium-1.0.27+mc1.20.1.jar)
- Updated CraftTweaker: CraftTweaker-fabric-1.20.1-14.0.28.jar -> CraftTweaker-fabric-1.20.1-14.0.30.jar

## 2.1.3
- Updated Botarium: botarium-fabric-1.20.1-2.2.1.jar -> botarium-fabric-1.20.1-2.2.2.jar
- Updated Create: Garnished: garnished-1.5+1.20.1.jar -> garnished-1.5.1-hotfix-1+1.20.1.jar
- Updated Enderman Tweaks: enderman_tweaks-0.2.0.jar -> enderman_tweaks-0.3.0.jar
- Updated FallingTree: FallingTree-1.20.1-4.3.3.jar -> FallingTree-1.20.1-4.3.4.jar
- Updated Memory Leak Fix: memoryleakfix-fabric-1.17+-1.1.2.jar -> memoryleakfix-fabric-1.17+-1.1.5.jar
- Updated More Culling: moreculling-1.20.1-0.19.0.jar -> moreculling-1.20.4-0.20.3.jar
- Updated Puzzles Lib: PuzzlesLib-v8.1.11-1.20.1-Fabric.jar -> PuzzlesLib-v8.1.12-1.20.1-Fabric.jar
- Updated Supplementaries: supplementaries-1.20-2.7.20-fabric.jar -> supplementaries-1.20-2.7.21-fabric.jar
- **Removed Create: Interactive** (seems it causes server lag atm, issues open in github repo also)

## 2.1.2
- Incrased max amount of connectors in network for Create: New Age from 32 to 128
- Updated CC: Tweaked: cc-tweaked-1.20.1-fabric-1.109.2.jar -> cc-tweaked-1.20.1-fabric-1.109.3.jar
- Updated Create: Garnished: garnished-1.4+1.20.1.jar -> garnished-1.5+1.20.1.jar
- Updated Moonlight Lib: moonlight-1.20-2.8.79-fabric.jar -> moonlight-1.20-2.8.80-fabric.jar
- Replaced some curseforge mods with the one on Modrinth:
  - Macaw's Bridges (mcw-bridges-2.1.0-mc1.20.1fabric.jar)
  - Macaw's Doors (mcw-doors-1.1.0fabric-mc1.20.1.jar)
  - Macaw's Fences and Walls (mcw-fences-1.0.7-mc1.20.1fabric.jar)
  - Macaw's Windows (mcw-windows-2.2.1-mc1.20.1fabric.jar)
  - MaLiLib (malilib-fabric-1.20.1-0.16.3.jar)
- **Added Chest Tracker** (chesttracker-1.20.1-1.1.23.jar)
- **Added Create Slice & Dice** (sliceanddice-fabric-3.0.0.jar)
- **Added CTOV - Farmer Delight Compat** (ctov-farmers-delight-compat-2.1.jar)
- **Added Farmer's Delight [Fabric]** (farmers-delight-fabric-1.4.3.jar)
- **Added Searchables** (Searchables-fabric-1.20.1-1.0.2.jar)
- Removed where-is-it (bundled in Chest Tracker)

## 2.1.1
- **Updated Fabric loader to version 0.15.3**
- Updated ImmediatelyFast: ImmediatelyFast-1.2.7+1.20.2.jar -> ImmediatelyFast-Fabric-1.2.8+1.20.4.jar
- Updated Supplementaries: supplementaries-1.20-2.7.17-fabric.jar -> supplementaries-1.20-2.7.20-fabric.jar
- Updated Where Is It: whereisit-2.2.6+1.20.1.jar -> whereisit-2.3.2+1.20.1.jar
- **Added Create Deco** (createdeco-2.0.0-fabric-1.20.1.jar)
- **Added Create Fabric Fixes** (createfabricfixes-0.0.2+1.20.1.jar)
- **Removed Bits And Chisels** (mod doesn't work with latest Fabric and author has been inactive for half a year)
- Added Chisels & Bits - For Fabric" (chisels-and-bits-fabric-1.4.145.jar)

## 2.1.0
> This update focuses in moving back to main version of Create: Fabric (away from the unofficial) & re-adding Valkyrien skies & Eureka (build ships & boats etc). Client performance might be worse due to removal of Sodium. It also adds awesome now mod Create:interactive: https://modrinth.com/mod/interactive - for example it enables to modify & interact fully with already assemebled contraptions & trains, meaning it is possible to create moving bases for example.

- Removed Sodium & Indium (until other mods are compatible with sodium 0.5 versions properly (create, euroka & valkyrien-skies), can't use older version because it requires old fabric-api and create-tracks-map does not support that)
- Removed create-fabric-sodium-fix
- Added Create Fabric (create-fabric-0.5.1-d-build.1161+mc1.20.1.jar)
- **Added Valkyrien Skies** (valkyrienskies-120-2.1.3-beta.1.jar)
- **Added Eureka!** (eureka-1201-1.3.0-beta.4.jar)
- **Added Create: Interactive** (create_interactive-1201-fabric-1.0.2-beta.1.jar)
- Updated Cloth Config API: cloth-config-11.1.106-fabric.jar -> cloth-config-11.1.118-fabric.jar
- Updated CraftTweaker: CraftTweaker-fabric-1.20.1-14.0.27.jar -> CraftTweaker-fabric-1.20.1-14.0.28.jar
- Updated Create: Garnished: garnished-1.3-hotfix-2+1.20.1.jar -> garnished-1.4+1.20.1.jar
- Updated Create: New Age: create-new-age-fabric-1.20.1-1.1.fix1.jar -> create-new-age-fabric-1.20.1-1.1.1.jar
- Updated Dynmap®: Dynmap-3.7-beta-3-fabric-1.20.jar -> Dynmap-3.7-beta-4-fabric-1.20.jar
- Updated Fabric Language Kotlin: fabric-language-kotlin-1.10.16+kotlin.1.9.21.jar -> fabric-language-kotlin-1.10.17+kotlin.1.9.22.jar
- Updated Jade 🔍: Jade-1.20-fabric-11.6.4.jar -> Jade-1.20-fabric-11.7.0.jar
- Updated ModernFix: modernfix-fabric-5.10.1+mc1.20.1.jar -> modernfix-fabric-5.11.1+mc1.20.1.jar
- Updated Moonlight Lib: moonlight-1.20-2.8.68-fabric.jar -> moonlight-1.20-2.8.79-fabric.jar
- Updated Patchouli: Patchouli-1.20.1-83-FABRIC.jar -> Patchouli-1.20.1-84-FABRIC.jar
- Updated Resourceful Lib: resourcefullib-fabric-1.20.1-2.1.19.jar -> resourcefullib-fabric-1.20.1-2.1.20.jar
- Updated Supplementaries: supplementaries-1.20-2.7.9-fabric.jar -> supplementaries-1.20-2.7.17-fabric.jar
- Updated Traveler's Backpack: travelers-backpack-1.20.1-9.1.8.jar -> travelers-backpack-1.20.1-9.1.9.jar

## 2.0.23
- Updated CC: Tweaked: cc-tweaked-1.20.1-fabric-1.109.0.jar -> cc-tweaked-1.20.1-fabric-1.109.2.jar
- Updated Create Enchantment Industry Fabric: create_enchantment_industry-1.2.10.jar -> create_enchantment_industry-1.2.11.jar
- Updated Create Jetpack: create_jetpack-fabric-4.0.0.jar -> create_jetpack-fabric-4.1.0.jar
- Updated Create: New Age: create-new-age-fabric-1.20.1-1.1.pre5.jar -> create-new-age-fabric-1.20.1-1.1.fix1.jar
- Updated Discord Integration: dcintegration-fabric-3.0.3-1.20.1.jar -> dcintegration-fabric-3.0.5-1.20.1.jar
- Updated Dynmap®: Dynmap-3.6-fabric-1.20.jar -> Dynmap-3.7-beta-3-fabric-1.20.jar
- Updated FallingTree: FallingTree-1.20.1-4.3.2.jar -> FallingTree-1.20.1-4.3.3.jar
- Updated Jade 🔍: Jade-1.20-fabric-11.6.2.jar -> Jade-1.20-fabric-11.6.4.jar
- Updated Macaw's Windows: mcw-windows-2.2.0-mc1.20.1fabric.jar -> mcw-windows-2.2.1-mc1.20.1fabric.jar
- Updated ModernFix: modernfix-fabric-5.10.0+mc1.20.1.jar -> modernfix-fabric-5.10.1+mc1.20.1.jar
- Updated Roughly Enough Items (REI): RoughlyEnoughItems-12.0.674.jar -> RoughlyEnoughItems-12.0.684-fabric.jar
- Updated Supplementaries: supplementaries-1.20-2.7.7-fabric.jar -> supplementaries-1.20-2.7.9-fabric.jar

## 2.0.22
- Update Fabric to 0.14.25**
- Added Create Enchantment Industry Fabric (create_enchantment_industry-1.2.10.jar)
- Added Default Options (defaultoptions-fabric-1.20-18.0.1.jar)
- Removed configured-defaults
- Updated Supplementaries: supplementaries-1.20-2.7.1-fabric.jar -> supplementaries-1.20-2.7.7-fabric.jar
- Updated Extended Drawers: ExtendedDrawers-2.0.8+mc.1.20.1.jar -> ExtendedDrawers-2.1.0+mc.1.20.1.jar
- Updated Balm: balm-fabric-1.20.1-7.1.4.jar -> balm-fabric-1.20.1-7.2.1.jar

## 2.0.20.1
- Reverted ImmediatelyFast to ImmediatelyFast-1.2.7+1.20.2.jar (needs newer Fabric version)

## 2.0.20
- Updated ImmediatelyFast: ImmediatelyFast-1.2.7+1.20.2.jar -> ImmediatelyFast-Fabric-1.2.8+1.20.4.jar
- Updated ChoiceTheorem's Overhauled Village: ctov-3.4-beta.jar -> ctov-3.3.6.jar
- Reverted Create: New Age to create-new-age-fabric-1.20.1-1.1.pre5.jar

## 2.0.19
- Updated ChoiceTheorem's Overhauled Village: ctov-3.3.6.jar -> ctov-3.4-beta.jar
- Updated Create: Garnished: garnished-1.2-hotfix-2+1.20.1.jar -> garnished-1.3-hotfix-2+1.20.1.jar
- Updated Create: New Age: create-new-age-fabric-1.20.1-1.1.pre5.jar -> create-new-age-fabric-1.20.1-1.1.jar
- Updated Fabric Language Kotlin: fabric-language-kotlin-1.10.15+kotlin.1.9.21.jar -> fabric-language-kotlin-1.10.16+kotlin.1.9.21.jar
- Updated Geckolib: geckolib-fabric-1.20.1-4.2.4.jar -> geckolib-fabric-1.20.1-4.3.1.jar
- Updated ModernFix: modernfix-fabric-5.9.3+mc1.20.1.jar -> modernfix-fabric-5.10.0+mc1.20.1.jar
- Updated Moonlight Lib: moonlight-1.20-2.8.67-fabric.jar -> moonlight-1.20-2.8.68-fabric.jar
- Updated Puzzles Lib: PuzzlesLib-v8.1.9-1.20.1-Fabric.jar -> PuzzlesLib-v8.1.11-1.20.1-Fabric.jar
- Updated Resourceful Lib: resourcefullib-fabric-1.20.1-2.1.18.jar -> resourcefullib-fabric-1.20.1-2.1.19.jar
- Updated SuperMartijn642's Core Lib: supermartijn642corelib-1.1.15-fabric-mc1.20.1.jar -> supermartijn642corelib-1.1.16-fabric-mc1.20.1.jar
- Updated Supplementaries: supplementaries-1.20-2.7.0-fabric.jar -> supplementaries-1.20-2.7.1-fabric.jar
- Updated Xaero's Minimap: Xaeros_Minimap_23.9.1_Fabric_1.20.jar -> Xaeros_Minimap_23.9.3_Fabric_1.20.jar
- Updated Xaero's World Map: XaerosWorldMap_1.37.1_Fabric_1.20.jar -> XaerosWorldMap_1.37.2_Fabric_1.20.jar

## 2.0.17.1
- Removed cupboard depedency that was left in

## 2.0.17
- Updated Xaero's Minimap: Xaeros_Minimap_23.9.0_Fabric_1.20.jar -> Xaeros_Minimap_23.9.1_Fabric_1.20.jar
- Updated Moonlight Lib: moonlight-1.20-2.8.66-fabric.jar -> moonlight-1.20-2.8.67-fabric.jar
- Updated CraftTweaker: CraftTweaker-fabric-1.20.1-14.0.26.jar -> CraftTweaker-fabric-1.20.1-14.0.27.jar
- Updated Supplementaries: supplementaries-1.20-2.6.30-fabric.jar -> supplementaries-1.20-2.7.0-fabric.jar
- Updated Xaero's World Map: XaerosWorldMap_1.37.0_Fabric_1.20.jar -> XaerosWorldMap_1.37.1_Fabric_1.20.jar
- Updated FallingTree: FallingTree-1.20.1-4.3.1.jar -> FallingTree-1.20.1-4.3.2.jar

## 2.0.16
- Updated Xaero's World Map: XaerosWorldMap_1.36.0_Fabric_1.20.jar -> XaerosWorldMap_1.37.0_Fabric_1.20.jar
- Updated Fabric API: fabric-api-0.90.7+1.20.1.jar -> fabric-api-0.91.0+1.20.1.jar
- Updated Fabric Language Kotlin: fabric-language-kotlin-1.10.14+kotlin.1.9.20.jar -> fabric-language-kotlin-1.10.15+kotlin.1.9.21.jar
- Updated Resourceful Lib: resourcefullib-fabric-1.20.1-2.1.16.jar -> resourcefullib-fabric-1.20.1-2.1.18.jar
- Updated Xaero's Minimap: Xaeros_Minimap_23.8.4_Fabric_1.20.jar -> Xaeros_Minimap_23.9.0_Fabric_1.20.jar

## 2.0.15
- Updated Moonlight Lib: moonlight-1.20-2.8.65-fabric.jar -> moonlight-1.20-2.8.66-fabric.jar
- Updated Create: Bells & Whistles: bellsandwhistles-0.4.4-hotfix+1.20.1-fabric.jar -> bellsandwhistles-0.4.5+1.20.1-FABRIC.jar

## 2.0.14
- Added Create: Bells & Whistles (bellsandwhistles-0.4.4-hotfix+1.20.1-fabric.jar)
- Updated Patchouli: Patchouli-1.20.1-81-FABRIC.jar -> Patchouli-1.20.1-83-FABRIC.jar
- Updated CraftTweaker: CraftTweaker-fabric-1.20.1-14.0.24.jar -> CraftTweaker-fabric-1.20.1-14.0.26.jar
- Updated Where Is It: whereisit-2.0.24+1.20.1.jar -> whereisit-2.2.6+1.20.1.jar

## 2.0.13
- Updated Magnum Torch: MagnumTorch-v8.0.0-1.20.1-Fabric.jar -> MagnumTorch-v8.0.1-1.20.1-Fabric.jar
- Updated Fusion (Connected Textures): fusion-1.1.0b-fabric-mc1.20.1.jar -> fusion-1.1.0c-fabric-mc1.20.1.jar
- Updated Create: New Age: create-new-age-fabric-1.20.1-1.1.pre4.jar -> create-new-age-fabric-1.20.1-1.1.pre5.jar
- Updated Puzzles Lib: PuzzlesLib-v8.1.8-1.20.1-Fabric.jar -> PuzzlesLib-v8.1.9-1.20.1-Fabric.jar
- Updated Carry On: carryon-fabric-1.20.1-2.1.2.5.jar -> carryon-fabric-1.20.1-2.1.2.7.jar
- Updated YUNG's Better Ocean Monuments: YungsBetterOceanMonuments-1.20-Fabric-3.0.3.jar -> YungsBetterOceanMonuments-1.20-Fabric-3.0.4.jar
- Updated Forgotten Graves: forgottengraves-1.20.1-3.2.4.jar -> forgottengraves-1.20.1-3.2.5.jar
- Updated Moonlight Lib: moonlight-1.20-2.8.62-fabric.jar -> moonlight-1.20-2.8.65-fabric.jar
- Updated CraftTweaker: CraftTweaker-fabric-1.20.1-14.0.23.jar -> CraftTweaker-fabric-1.20.1-14.0.24.jar

## 2.0.12
- Remove YOSBR (replaced with configured defaults)
- Added Configured Defaults (ConfiguredDefaults-v8.0.1-1.20.1-Fabric.jar)
- Updated Supplementaries: supplementaries-1.20-2.6.29-fabric.jar -> supplementaries-1.20-2.6.30-fabric.jar

## 2.0.11
- Updated Fabric Language Kotlin: fabric-language-kotlin-1.10.13+kotlin.1.9.20.jar -> fabric-language-kotlin-1.10.14+kotlin.1.9.20.jar
- Updated Supplementaries: supplementaries-1.20-2.6.28-fabric.jar -> supplementaries-1.20-2.6.29-fabric.jar
- Increase nature's compass and explorer's compass ranges to 50k (from 10k)
- Disable litematica tool by default

## 2.0.10
- Added Universal Shops (universal_shops-1.3.2+1.20.1.jar)
- Updated Moonlight Lib: moonlight-1.20-2.8.60-fabric.jar -> moonlight-1.20-2.8.62-fabric.jar
- Updated Supplementaries: supplementaries-1.20-2.6.26-fabric.jar -> supplementaries-1.20-2.6.28-fabric.jar
- Updated CC: Tweaked: cc-tweaked-1.20.1-fabric-1.108.4.jar -> cc-tweaked-1.20.1-fabric-1.109.0.jar
- Updated Fusion (Connected Textures): fusion-1.1.0a-fabric-mc1.20.1.jar -> fusion-1.1.0b-fabric-mc1.20.1.jar

## 2.0.9
- Configured Traveler's Backpack to use trinkets and made adjustments given by the Forgotten Graves author to make it more compatible with it
  - Should fix the issue of backpack getting destroyed on death, it should go into the grave now
- Updated Supplementaries: supplementaries-1.20-2.6.24-fabric.jar -> supplementaries-1.20-2.6.26-fabric.jar
- Updated ModernFix: modernfix-fabric-5.9.2+mc1.20.1.jar -> modernfix-fabric-5.9.3+mc1.20.1.jar
- Updated Create: New Age: create-new-age-fabric-1.20.1-1.1.pre3.jar -> create-new-age-fabric-1.20.1-1.1.pre4.jar
- Updated BetterF3: BetterF3-7.0.1-Fabric-1.20.1.jar -> BetterF3-7.0.2-Fabric-1.20.1.jar

## 2.0.8-s1
- Configured /tps command in Discord

## 2.0.8
- Added spark (spark-1.10.53-fabric.jar)
- Removed bodies-bodies
- Re-added Carry On (carryon-fabric-1.20.1-2.1.2.5.jar) (bodies-bodies got removed)
- Added Forgotten Graves (forgottengraves-1.20.1-3.2.4.jar)
- Added Create: Garnished (garnished-1.2-hotfix-2+1.20.1.jar)

## 2.0.7
- Remove paladins-furniture as the mod author broke it

## 2.0.6
- Fix paladins furniture mod files changing
- Updated CraftTweaker: CraftTweaker-fabric-1.20.1-14.0.21.jar -> CraftTweaker-fabric-1.20.1-14.0.23.jar
- Updated Supplementaries: supplementaries-1.20-2.6.20-fabric.jar -> supplementaries-1.20-2.6.24-fabric.jar
- Updated Fusion (Connected Textures): fusion-1.1.0-fabric-mc1.20.1.jar -> fusion-1.1.0a-fabric-mc1.20.1.jar
- Updated Extended Drawers: ExtendedDrawers-2.0.7+mc.1.20.1.jar -> ExtendedDrawers-2.0.8+mc.1.20.1.jar

## 2.0.5.1
- Added receipe to craft a saddle

## 2.0.5
- Removed Carry On (seems to have compatibility issues woth Bodies! Bodies! mod)
- Added Paladin's Furniture Mod (paladin-furniture-mod-1.2.0-fabric-mc1.20.1.jar)
- Updated Resourceful Lib: resourcefullib-fabric-1.20.1-2.1.14.jar -> resourcefullib-fabric-1.20.1-2.1.16.jar
- Updated Fusion (Connected Textures): fusion-1.0.6-fabric-mc1.20.jar -> fusion-1.1.0-fabric-mc1.20.1.jar
- Updated ImmediatelyFast: ImmediatelyFast-1.2.6+1.20.2.jar -> ImmediatelyFast-1.2.7+1.20.2.jar
- Updated Moonlight Lib: moonlight-1.20-2.8.57-fabric.jar -> moonlight-1.20-2.8.60-fabric.jar
- Updated CraftTweaker: CraftTweaker-fabric-1.20.1-14.0.18.jar -> CraftTweaker-fabric-1.20.1-14.0.21.jar

## 2.0.4
- Removed Gravestones mods
- Added Bodies! Bodies! (BodiesBodies-1.0.1.jar)
- Fix Discord integration whitelist command so that we can use list command also

## 2.0.3
- Remove Amecs (issues with shift and ctrl keys)
- Keybind Fix (keybind_fix-1.0.0.jar)

## 2.0.2-s2
- Readd c2me-fabric
- Removed login-protection as that mods issues include multiple invincibility bugs, I'm hoping this mod is causing our issues instead

## 2.0.2-s1
- Increase Gotta go too fast limits (away from vanialla minecraft)
- Removed c2me-fabric to see if that causes the issue of players getting invincible

## 2.0.2-c1
- Fix xaero minimap config being overwritten
- Update server name in the default servers.dat

## 2.0.2
- Updated CraftTweaker: CraftTweaker-fabric-1.20.1-14.0.17.jar -> CraftTweaker-fabric-1.20.1-14.0.18.jar
- Updated Create: New Age: create-new-age-fabric-1.20.1-1.1.pre1.jar -> create-new-age-fabric-1.20.1-1.1.pre3.jar
- Updated Jade Addons (Fabric): JadeAddons-1.20.1-fabric-5.2.2.jar -> JadeAddons-1.20.1-fabric-5.2.3.jar
- Updated ModernFix: modernfix-fabric-5.9.0+mc1.20.1.jar -> modernfix-fabric-5.9.2+mc1.20.1.jar
- Updated Moonlight Lib: moonlight-1.20-2.8.56-fabric.jar -> moonlight-1.20-2.8.57-fabric.jar
- Updated Naturalist: naturalist-fabric-4.0.1-1.20.1.jar -> naturalist-fabric-4.0.3-1.20.1.jar
- Updated Puzzles Lib: PuzzlesLib-v8.1.7-1.20.1-Fabric.jar -> PuzzlesLib-v8.1.8-1.20.1-Fabric.jar
- Updated Resourceful Lib: resourcefullib-fabric-1.20.1-2.1.13.jar -> resourcefullib-fabric-1.20.1-2.1.14.jar
- Updated Supplementaries: supplementaries-1.20-2.6.19-fabric.jar -> supplementaries-1.20-2.6.20-fabric.jar
- Added Explorer's Compass (ExplorersCompass-1.20.1-2.2.3-fabric.jar) 
- Added Enderman Tweaks (enderman_tweaks-0.2.0.jar) to disable enderman from picking up and placing blocks

## 2.0.1
- Added Open Simple Frames (OpenSimpleFrames-1.1.0.jar)

## 2.0.0
- **Updated to MC 1.20.1**
- **Updated to Fabric 0.14.24**
- **Renamed modpack to Transportcraft Next Generation** - Lite didn't feel like it fits the scope of the modpack for quite some time now
---
- Updated:
  - Litematica: litematica-fabric-1.19.2-0.12.7.jar -> litematica-fabric-1.20.1-0.15.3.jar
  - Macaw's Doors: mcw-doors-1.1.0fabric-mc1.19.2.jar -> mcw-doors-1.1.0fabric-mc1.20.1.jar
  - Macaw's Windows: mcw-windows-2.2.0-mc1.19.2fabric.jar -> mcw-windows-2.2.0-mc1.20.1fabric.jar
  - MiniHUD: minihud-fabric-1.19.2-0.23.3.jar -> minihud-fabric-1.20.1-0.27.0.jar
  - Macaw's Fences and Walls: mcw-fences-1.0.7-mc1.19.2fabric.jar -> mcw-fences-1.0.7-mc1.20.1fabric.jar
  - MaLiLib: malilib-fabric-1.19.2-0.13.0.jar -> malilib-fabric-1.20.1-0.16.3.jar
  - Tweakeroo: tweakeroo-fabric-1.19.2-0.14.3.jar -> tweakeroo-fabric-1.20.1-0.17.1.jar
  - Roughly Enough Resources: rer-2.6.0.jar -> rer-2.9.0.jar
  - Login Protection[Forge/Fabric]: logprot-fabric-1.19-2.0.jar -> logprot-fabric-1.20.1-3.1.jar
  - Puzzles Lib: PuzzlesLib-v4.4.3-1.19.2-Fabric.jar -> PuzzlesLib-v8.1.5-1.20.1-Fabric.jar
  - BetterF3: BetterF3-4.0.0-Fabric-1.19.2.jar -> BetterF3-7.0.1-Fabric-1.20.1.jar
  - Cloth Config API: cloth-config-8.3.103-fabric.jar -> cloth-config-11.1.106-fabric.jar
  - Krypton: krypton-0.2.1.jar -> krypton-0.2.3.jar
  - Mod Menu: modmenu-4.2.0-beta.2.jar -> modmenu-7.2.2.jar
  - Rechiseled: rechiseled-1.1.5-fabric-mc1.19.2.jar -> rechiseled-1.1.5b-fabric-mc1.20.jar
  - Fabric Language Kotlin: fabric-language-kotlin-1.10.10+kotlin.1.9.10.jar -> fabric-language-kotlin-1.10.13+kotlin.1.9.20.jar
  - Inventory Essentials: inventoryessentials-fabric-1.19.2-5.4.0.jar -> inventoryessentials-fabric-1.20.1-8.2.1.jar
  - Valkyrien Skies: valkyrienskies-1192-2.2.0-beta.3.jar -> valkyrienskies-1201-2.3.0-beta.4.jar
  - Dark Loading Screen: dark-loading-screen-1.6.12.jar -> dark-loading-screen-1.6.14.jar
  - Patchouli: Patchouli-1.19.2-77-FABRIC.jar -> Patchouli-1.20.1-81-FABRIC.jar
  - Traveler's Backpack: travelers-backpack-1.19.2-8.2.30.jar -> travelers-backpack-1.20.1-9.1.8.jar
  - Fusion (Connected Textures): fusion-1.0.6-fabric-mc1.19.2.jar -> fusion-1.0.6-fabric-mc1.20.jar
  - Dynmap®: Dynmap-3.6-fabric-1.19.1.jar -> Dynmap-3.6-fabric-1.20.jar
  - Fabric API: fabric-api-0.76.1+1.19.2.jar -> fabric-api-0.90.7+1.20.1.jar
  - FerriteCore: ferritecore-5.0.3-fabric.jar -> ferritecore-6.0.1-fabric.jar
  - Mouse Tweaks: MouseTweaks-fabric-mc1.19-2.22.jar -> MouseTweaks-fabric-mc1.20-2.25.jar
  - Sodium: sodium-fabric-mc1.19.2-0.4.4+build.18.jar -> sodium-fabric-mc1.20-0.4.10+build.27.jar
  - Carpet: fabric-carpet-1.19.2-1.4.84+v221018.jar -> fabric-carpet-1.20-1.4.112+v230608.jar
  - Xaero's World Map: XaerosWorldMap_1.35.0_Fabric_1.19.1.jar -> XaerosWorldMap_1.36.0_Fabric_1.20.jar
  - bad packets: badpackets-fabric-0.2.3.jar -> badpackets-fabric-0.4.3.jar
  - Create Jetpack: create_jetpack-fabric-3.3.1.jar -> create_jetpack-fabric-4.0.0.jar
  - YetAnotherConfigLib: YetAnotherConfigLib-2.2.0-for-1.19.2.jar -> yet-another-config-lib-fabric-3.2.1+1.20.jar
  - Carry On: carryon-fabric-1.19.2-2.1.1.21.jar -> carryon-fabric-1.20.1-2.1.2.5.jar
  - Gravestones: gravestones-v1.13.jar -> gravestones-v1.15.jar
  - Magnum Torch: MagnumTorch-v4.2.3-1.19.2-Fabric.jar -> MagnumTorch-v8.0.0-1.20.1-Fabric.jar
  - Enchanted Vertical Slabs: mc1.19-enchanted-vertical-slabs-1.8.jar -> enchanted-vertical-slabs-1.9.1.jar
  - Lithium: lithium-fabric-mc1.19.2-0.11.1.jar -> lithium-fabric-mc1.20.1-0.11.2.jar
  - Syncmatica: syncmatica-1.18.2-0.3.10.jar -> syncmatica-1.20.1-0.3.11.jar
  - Better Mount HUD: bettermounthud-1.2.0.jar -> bettermounthud-1.2.2.jar
  - Create Crafts & Additions: createaddition-fabric+1.19.2-20230723a.jar -> createaddition-fabric+1.20.1-0.9.0.jar
  - Iris Shaders: iris-mc1.19.2-1.6.10.jar -> iris-mc1.20.1-1.6.10.jar
  - Create: Steam n’ Rails: Steam_Rails-1.5.1+fabric-mc1.19.2.jar -> Steam_Rails-1.5.1+fabric-mc1.20.1.jar
  - Faux Custom Entity Data: FauxCustomEntityData-fabric-1.19.2-2.0.2.jar -> FauxCustomEntityData-fabric-1.20.1-6.0.1.jar
  - Inventory Sorting: InventorySorter-1.8.10-1.19.jar -> InventorySorter-1.9.0-1.20.jar
  - Trinkets: trinkets-3.4.2.jar -> trinkets-3.7.1.jar
  - Building Wands: BuildingWands-mc1.19.2-2.6.8-release-fabric.jar -> BuildingWands-mc1.20.1-2.6.8-release-fabric.jar
  - Bobby: bobby-4.0.1.jar -> bobby-5.0.0.jar
  - Dynamic Music Updated: dynmus-v2.3.1+1.19.2-fabric.jar -> dynmus-v2.3.2+1.20-fabric.jar
  - Eureka!: eureka-1192-1.2.0-beta.1.jar -> eureka-1201-1.3.0-beta.2.jar
  - FallingTree: FallingTree-1.19.2-3.10.0.jar -> FallingTree-1.20.1-4.3.1.jar
  - Polymorph: polymorph-fabric-0.46.4+1.19.2.jar -> polymorph-fabric-0.49.1+1.20.1.jar
  - SuperMartijn642's Core Lib: supermartijn642corelib-1.1.15-fabric-mc1.19.2.jar -> supermartijn642corelib-1.1.15-fabric-mc1.20.1.jar
  - Clumps: Clumps-fabric-1.19.2-9.0.0+14.jar -> Clumps-fabric-1.20.1-12.0.0.3.jar
  - Create Fabric: create-fabric-0.5.1-c-build.1160+mc1.19.2.jar -> create-fabric-0.5.1-d-build.1161+mc1.20.1.jar
  - SuperMartijn642's Config Lib: supermartijn642configlib-1.1.8a-fabric-mc1.19.jar -> supermartijn642configlib-1.1.8a-fabric-mc1.20.jar
  - CraftTweaker: CraftTweaker-fabric-1.19.2-10.1.50.jar -> CraftTweaker-fabric-1.20.1-14.0.17.jar
  - Balm: balm-fabric-1.19.2-4.6.0.jar -> balm-fabric-1.20.1-7.1.4.jar
  - Moonlight Lib: moonlight-1.19.2-2.2.45-fabric.jar -> moonlight-1.20-2.8.56-fabric.jar
  - Supplementaries: supplementaries-1.19.2-2.3.24-fabric.jar -> supplementaries-1.20-2.6.19-fabric.jar
  - Jade 🔍: Jade-1.19.1-fabric-8.7.3.jar -> Jade-1.20-fabric-11.6.2.jar
  - Better Compatibility Checker: BetterCompatibilityChecker-Fabric-2.0.2-build.16+mc1.19.1.jar -> BetterCompatibilityChecker-fabric-4.0.1+mc1.20.1.jar
  - Gotta Go Fast [Fabric]: gottagofast-1.1.0+1.19.2.jar -> gottagofast-1.1.0.jar
  - Indium: indium-1.0.9+mc1.19.2.jar -> indium-1.0.21+mc1.20.1.jar
  - Architectury API: architectury-6.5.85-fabric.jar -> architectury-9.1.12-fabric.jar
  - Enhanced Block Entities: enhancedblockentities-0.7.2+1.19.2.jar -> enhancedblockentities-0.9+1.20.jar
  - Forge Config API Port: ForgeConfigAPIPort-v4.2.11-1.19.2-Fabric.jar -> ForgeConfigAPIPort-v8.0.0-1.20.1-Fabric.jar
  - Xaero's Minimap: Xaeros_Minimap_23.8.3_Fabric_1.19.1.jar -> Xaeros_Minimap_23.8.4_Fabric_1.20.jar
  - Naturalist: naturalist-fabric-3.0.4-1.19.2.jar -> naturalist-fabric-4.0.1-1.20.1.jar
  - Applied Energistics 2: appliedenergistics2-fabric-12.9.8.jar -> appliedenergistics2-fabric-15.0.14.jar
  - Iron Chests: IronChests-1.7.7.jar -> IronChests-2.0.1.jar
  - Geckolib: geckolib-fabric-1.19-3.1.40.jar -> geckolib-fabric-1.20.1-4.2.4.jar
  - Debugify: Debugify-2.8.0.jar -> Debugify-1.20.1+2.0.jar
  - Powah!: Powah-4.0.11.jar -> Powah-5.0.2.jar
  - oωo (owo-lib): owo-lib-0.9.3+1.19.jar -> owo-lib-0.11.2+1.20.jar
  - Amecs: amecs-1.3.8+mc.1.19-rc2.jar -> amecs-1.3.10+mc.1.20.1.jar
  - Extended Drawers: ExtendedDrawers-1.4.5+mc.1.19.2.jar -> ExtendedDrawers-2.0.7+mc.1.20.1.jar
  - Rechiseled: Create: rechiseledcreate-1.0.1-fabric-mc1.19.jar -> rechiseledcreate-1.0.1-fabric-mc1.20.jar
  - Roughly Enough Items (REI): RoughlyEnoughItems-9.1.663.jar -> RoughlyEnoughItems-12.0.674.jar
  - AppleSkin: appleskin-fabric-mc1.19-2.4.1.jar -> appleskin-fabric-mc1.20.1-2.5.1.jar
  - No Chat Reports: NoChatReports-FABRIC-1.19.2-v1.13.12.jar -> NoChatReports-FABRIC-1.20.1-v2.2.2.jar
  - CreateFabric&REIBugFix: CreateFabricREIBugFix-0.1.5-create0.5.1-mc1.19.x.jar -> CreateFabricREIBugFix-0.1.0-create0.5.1-mc1.20.x.jar
  - Bad Wither No Cookie - Reloaded: bwncr-fabric-1.19.2-3.14.1.jar -> bwncr-fabric-1.20.1-3.17.0.jar
  - Jade Addons (Fabric): JadeAddons-1.19.2-fabric-3.2.0.jar -> JadeAddons-1.20.1-fabric-5.2.2.jar
  - MidnightLib: midnightlib-fabric-1.0.0.jar -> midnightlib-fabric-1.4.1.1.jar
  - Pick Up Notifier: PickUpNotifier-v4.2.4-1.19.2-Fabric.jar -> PickUpNotifier-v8.0.0-1.20.1-Fabric.jar
  - Create Track Map: create-track-map-1.4+mc1.19.2-fabric-create-0.5.1.jar -> create-track-map-1.4+mc1.20.1-fabric.jar
  - JamLib: jamlib-0.6.1+1.19-1.19.2.jar -> jamlib-0.6.1+1.20.x.jar

- Removed:
  - advancedchatcore
  - cc-restitched (Replaced with CC: Tweaked)
  - editsign (Built into MC since 1.20)
  - cammies-minecart-tweaks
  - ingredient-extension-api (not even sure what used it)
  - decorative-blocks (curseforge version)
  - phosphor (not compatible with 1.20 and supposedly not needed anymore)
  - Iris Shaders (none of us are actually using it)
  - cyberanner-ironchest (buggy and we have other storage solutions)
  - create-fabric (replaced with Create Fabric Sodium Fix due to sodium update)
  - eureka & valkyrien-skies (not working in our server atm)
  - ae2
  - powah
  - rechiseled & rechiseled: create (replaced with chipped as it has a lot more blocks)
  - Paladin's Furniture Mod (no public release for 1.20 yet, should come out soon)

- Added:
  - CC: Tweaked (cc-tweaked-1.20.1-fabric-1.108.4.jar)
  - Decorative Blocks (Decorative Blocks-fabric-1.20.1-4.0.3.jar)
  - Create: Steam 'n' Rails (Steam_Rails-1.5.1+fabric-mc1.20.1.jar)
  - Create: New Age (create-new-age-fabric-1.20.1-1.1.pre1.jar)
  - Create: Structures (create-structures-0.1.1-1.20.1-FABRIC.jar)
  - Chunk Loaders (chunkloaders-1.2.8a-fabric-mc1.20.1.jar)
  - ModernFix (modernfix-fabric-5.9.0+mc1.20.1.jar)
  - FerriteCore (ferritecore-6.0.1-fabric.jar)
  - ImmediatelyFast (ImmediatelyFast-1.2.6+1.20.2.jar)
  - Krypton (krypton-0.2.3.jar)
  - More Culling (moreculling-1.20.1-0.19.0.jar)
  - Carpet-Fixes (carpet-fixes-1.20-1.16.1.jar)
  - Concurrent Chunk Management Engine (Fabric) (c2me-fabric-mc1.20.1-0.2.0+alpha.11.0.jar)
  - Enhanced Block Entities (enhancedblockentities-0.9+1.20.jar)
  - Botarium (botarium-fabric-1.20.1-2.2.1.jar)
  - ChoiceTheorem's Overhauled Village (ctov-3.3.6.jar)
  - CTOV - Create: Structures (ctov-create-structures-1.0.jar)
  - Servux (servux-fabric-1.20.0-0.1.0.jar)
  - YUNG's API (YungsApi-1.20-Fabric-4.0.2.jar)
  - YUNG's Better Strongholds (YungsBetterStrongholds-1.20-Fabric-4.0.3.jar)
  - YUNG's Better Desert Temples (YungsBetterDesertTemples-1.20-Fabric-3.0.3.jar)
  - YUNG's Better Ocean Monuments (YungsBetterOceanMonuments-1.20-Fabric-3.0.3.jar)
  - YUNG's Better Mineshafts (YungsBetterMineshafts-1.20-Fabric-4.0.4.jar)
  - YUNG's Better Dungeons (YungsBetterDungeons-1.20-Fabric-4.0.3.jar)
  - YUNG's Extras (YungsExtras-1.20-Fabric-4.0.3.jar)
  - YUNG's Better Witch Huts (YungsBetterWitchHuts-1.20-Fabric-3.0.3.jar)
  - YUNG's Bridges (YungsBridges-1.20-Fabric-4.0.3.jar)
  - YUNG's Better Nether Fortresses (YungsBetterNetherFortresses-1.20-Fabric-2.0.5.jar)
  - YUNG's Better Jungle Temples (YungsBetterJungleTemples-1.20-Fabric-2.0.4.jar)
  - Wildlife [Fabric] (wildlife-1.20.1-2.2.jar)
  - Companion (Companion-1.20-fabric-5.1.0.jar)
  - Nature's Compass (NaturesCompass-1.20.1-2.2.3-fabric.jar)
  - Cardinal Components API (cardinal-components-api-5.2.2.jar)
  - Utility Belt (utilitybelt-1.3.6+1.20.1.jar)
  - NiftyCarts (niftycarts-3.0.0+1.20.1.jar)
  - Slime (slime-1.5.jar)
  - Where Is It (whereisit-2.0.24+1.20.1.jar)
  - Macaw's Bridges (mcw-bridges-2.1.0-mc1.20.1fabric.jar)
  - Convenient Name Tags (convenientnametags-1.1.0.jar)
  - Bits And Chisels (bitsandchisels-2.7.3.jar)
  - Immersive Paintings (immersive_paintings-0.6.7+1.20.1-fabric.jar)
  - Resourceful Lib (resourcefullib-fabric-1.20.1-2.1.13.jar)
  - Athena (athena-fabric-1.20.1-3.1.1.jar)
  - Chipped (chipped-fabric-1.20.1-3.0.1.jar)
  - Scarecrows' Territory (scarecrowsterritory-1.1.11-fabric-mc1.20.jar)
  - William Wythers' Overhauled Overworld (WWOO-FABRIC+FORGE+QUILT-2.0.0.jar)

## 1.1.6
- Added Better Compatibility Checker (BetterCompatibilityChecker-Fabric-2.0.2-build.16+mc1.19.1.jar)
- Updated SuperMartijn642's Core Lib: supermartijn642corelib-1.1.12b-fabric-mc1.19.2.jar -> supermartijn642corelib-1.1.15-fabric-mc1.19.2.jar
- Updated Rechiseled: Create: rechiseledcreate-1.0.0-fabric-mc1.19.jar -> rechiseledcreate-1.0.1-fabric-mc1.19.jar
- Updated Xaero's Minimap: Xaeros_Minimap_23.8.0_Fabric_1.19.1.jar -> Xaeros_Minimap_23.8.3_Fabric_1.19.1.jar
- Updated SuperMartijn642's Config Lib: supermartijn642configlib-1.1.8-fabric-mc1.19.jar -> supermartijn642configlib-1.1.8a-fabric-mc1.19.jar
- Updated CraftTweaker: CraftTweaker-fabric-1.19.2-10.1.49.jar -> CraftTweaker-fabric-1.19.2-10.1.50.jar
- Updated Extended Drawers: ExtendedDrawers-1.4.3+mc.1.19.2.jar -> ExtendedDrawers-1.4.5+mc.1.19.2.jar
- Updated Roughly Enough Items (REI): RoughlyEnoughItems-9.1.657.jar -> RoughlyEnoughItems-9.1.663.jar
- Updated Gotta Go Fast [Fabric]: gottagofast-1.0.2-1.19.2.jar -> gottagofast-1.1.0+1.19.2.jar
- Updated Rechiseled: rechiseled-1.1.4-fabric-mc1.19.2.jar -> rechiseled-1.1.5-fabric-mc1.19.2.jar
- Updated Fusion (Connected Textures): fusion-1.0.5-fabric-mc1.19.2.jar -> fusion-1.0.6-fabric-mc1.19.2.jar
- Updated Iris Shaders: iris-mc1.19.2-1.6.9.jar -> iris-mc1.19.2-1.6.10.jar
- Updated Traveler's Backpack: travelers-backpack-1.19.2-8.2.28.jar -> travelers-backpack-1.19.2-8.2.30.jar
- Updated Xaero's World Map: XaerosWorldMap_1.34.1_Fabric_1.19.1.jar -> XaerosWorldMap_1.35.0_Fabric_1.19.1.jar
- Updated bad packets: badpackets-fabric-0.2.2.jar -> badpackets-fabric-0.2.3.jar
- Replaced bad packets with Modrinth version

## 1.1.5
- Updated bad packets: badpackets-fabric-0.2.1.jar -> badpackets-fabric-0.2.2.jar
- Updated Macaw's Windows: mcw-windows-2.1.2-mc1.19.2fabric.jar -> mcw-windows-2.2.0-mc1.19.2fabric.jar
- Updated Iris Shaders: iris-mc1.19.2-1.6.6.jar -> iris-mc1.19.2-1.6.9.jar
- Updated Xaero's World Map: XaerosWorldMap_1.34.0_Fabric_1.19.1.jar -> XaerosWorldMap_1.34.1_Fabric_1.19.1.jar
- Updated Roughly Enough Items (REI): RoughlyEnoughItems-9.1.650.jar -> RoughlyEnoughItems-9.1.657.jar
- Updated Create: Steam n’ Rails: Steam_Rails-1.5.0+fabric-mc1.19.2-build.14.jar -> Steam_Rails-1.5.1+fabric-mc1.19.2.jar
- Updated Inventory Essentials: inventoryessentials-fabric-1.19-5.0.2.jar -> inventoryessentials-fabric-1.19.2-5.4.0.jar
- Updated Xaero's Minimap: Xaeros_Minimap_23.7.0_Fabric_1.19.1.jar -> Xaeros_Minimap_23.8.0_Fabric_1.19.1.jar
- Updated Balm: balm-fabric-1.19.2-4.5.7.jar -> balm-fabric-1.19.2-4.6.0.jar
- Updated Rechiseled: rechiseled-1.1.2-fabric-mc1.19.2.jar -> rechiseled-1.1.4-fabric-mc1.19.2.jar
- Updated Moonlight Lib: moonlight-1.19.2-2.2.44-fabric.jar -> moonlight-1.19.2-2.2.45-fabric.jar
- Updated Traveler's Backpack: travelers-backpack-1.19.2-8.2.26.jar -> travelers-backpack-1.19.2-8.2.28.jar

## 1.1.4
- Updated Create Jetpack: create_jetpack-fabric-3.2.3.jar -> create_jetpack-fabric-3.3.1.jar
- Updated Create Fabric: create-fabric-0.5.1-b-build.1089+mc1.19.2.jar -> create-fabric-0.5.1-c-build.1160+mc1.19.2.jar
- Updated Carry On: carryon-fabric-1.19.2-2.1.0.20.jar -> carryon-fabric-1.19.2-2.1.1.21.jar
- Updated Applied Energistics 2: appliedenergistics2-fabric-12.9.7.jar -> appliedenergistics2-fabric-12.9.8.jar
- Updated Fabric Language Kotlin: fabric-language-kotlin-1.10.9+kotlin.1.9.0.jar -> fabric-language-kotlin-1.10.10+kotlin.1.9.10.jar
- Updated Memory Leak Fix: memoryleakfix-fabric-1.17+-1.1.1.jar -> memoryleakfix-fabric-1.17+-1.1.2.jar
- Updated Xaero's Minimap: Xaeros_Minimap_23.6.3_Fabric_1.19.1.jar -> Xaeros_Minimap_23.7.0_Fabric_1.19.1.jar
- Updated Create: Steam n’ Rails: Steam_Rails-1.4.3+fabric-mc1.19.2.jar -> Steam_Rails-1.5.0+fabric-mc1.19.2-build.14.jar
- Updated Powah!: Powah-4.0.10.jar -> Powah-4.0.11.jar
- Updated Roughly Enough Items (REI): RoughlyEnoughItems-9.1.643.jar -> RoughlyEnoughItems-9.1.650.jar
- Updated SuperMartijn642's Core Lib: supermartijn642corelib-1.1.12-fabric-mc1.19.2.jar -> supermartijn642corelib-1.1.12b-fabric-mc1.19.2.jar
- Updated Xaero's World Map: XaerosWorldMap_1.33.1_Fabric_1.19.1.jar -> XaerosWorldMap_1.34.0_Fabric_1.19.1.jar

## 1.1.3
- Updated Xaero's World Map: XaerosWorldMap_1.32.0_Fabric_1.19.1.jar -> XaerosWorldMap_1.33.1_Fabric_1.19.1.jar
- Updated Supplementaries: supplementaries-1.19.2-2.3.20-fabric.jar -> supplementaries-1.19.2-2.3.24-fabric.jar
- Updated Xaero's Minimap: Xaeros_Minimap_23.6.1_Fabric_1.19.1.jar -> Xaeros_Minimap_23.6.3_Fabric_1.19.1.jar
- Updated Fabric API: fabric-api-0.76.0+1.19.2.jar -> fabric-api-0.76.1+1.19.2.jar
- Updated Iris Shaders: iris-mc1.19.2-1.6.5.jar -> iris-mc1.19.2-1.6.6.jar
- Updated Applied Energistics 2: appliedenergistics2-fabric-12.9.6.jar -> appliedenergistics2-fabric-12.9.7.jar
- Updated Fabric Language Kotlin: fabric-language-kotlin-1.10.8+kotlin.1.9.0.jar -> fabric-language-kotlin-1.10.9+kotlin.1.9.0.jar
- Updated SuperMartijn642's Config Lib: supermartijn642configlib-1.1.7-fabric-mc1.19.jar -> supermartijn642configlib-1.1.8-fabric-mc1.19.jar

## 1.1.2
- **Updated Fabric loader to version 0.14.22**
- Updated Moonlight Lib: moonlight-1.19.2-2.2.43-fabric.jar -> moonlight-1.19.2-2.2.44-fabric.jar
- Updated Roughly Enough Items (REI): RoughlyEnoughItems-9.1.632.jar -> RoughlyEnoughItems-9.1.643.jar
- Updated Xaero's Minimap: Xaeros_Minimap_23.6.0_Fabric_1.19.1.jar -> Xaeros_Minimap_23.6.1_Fabric_1.19.1.jar
- Updated Iris Shaders: iris-mc1.19.2-1.6.4.jar -> iris-mc1.19.2-1.6.5.jar
- Updated Xaero's World Map: XaerosWorldMap_1.31.0_Fabric_1.19.1.jar -> XaerosWorldMap_1.32.0_Fabric_1.19.1.jar
- Updated RightClickHarvest: right-click-harvest-3.2.2+1.19.x-1.20.1-fabric.jar -> right-click-harvest-3.2.3+1.19.x-1.20.1-fabric.jar

## 1.1.1
- Updated Syncmatica: syncmatica-1.18.2-0.3.8.jar -> syncmatica-1.18.2-0.3.10.jar
- Updated Carry On: carryon-fabric-1.19.2-2.1.0.19.jar -> carryon-fabric-1.19.2-2.1.0.20.jar
- Updated JamLib: jamlib-0.6.0+1.19.jar -> jamlib-0.6.1+1.19-1.19.2.jar
- Updated CraftTweaker: CraftTweaker-fabric-1.19.2-10.1.48.jar -> CraftTweaker-fabric-1.19.2-10.1.49.jar
- Updated Magnum Torch: MagnumTorch-v4.2.2-1.19.2-Fabric.jar -> MagnumTorch-v4.2.3-1.19.2-Fabric.jar
- Added FerriteCore (ferritecore-5.0.3-fabric.jar)
- Added Enhanced Block Entities (enhancedblockentities-0.7.2+1.19.2.jar)
- Added SuperMartijn642's Config Lib (supermartijn642configlib-1.1.7-fabric-mc1.19.jar)
- Added Fusion (Connected Textures) (fusion-1.0.5-fabric-mc1.19.2.jar)
- Added SuperMartijn642's Core Lib (supermartijn642corelib-1.1.12-fabric-mc1.19.2.jar)
- Added Rechiseled (rechiseled-1.1.2-fabric-mc1.19.2.jar)
- Added Rechiseled: Create (rechiseledcreate-1.0.0-fabric-mc1.19.jar)

## 1.1.0
- **Update to Fabric 0.14.21**
- Moving modpack to modrinth (thus mods have been updated)
- **Added Clumps (closes [issue#35](https://gitlab.com/thesociety/transportcraft-lite/-/issues/#35)) (Clumps-fabric-1.19.2-9.0.0+14.jar)**
- Replace Farsight with Bobby (bobby-4.0.1.jar)
- Replace Iron Jetpacks with Create Jetpack (create_jetpack-fabric-3.2.1.jar)
- Reworked transportation mods (closes [issue#33](https://gitlab.com/thesociety/transportcraft-lite/-/issues/33))
  - **Added Valkyrien Skies (valkyrienskies-1192-2.2.0-beta.2.jar))**
  - **Added Eureka! (eureka-1192-1.2.0-beta.1.jar)**
  - Remove Automobility
  - Remove Small Ships
  - Remove Immersive Aircraft
- **Added Create Track Map (create-track-map-1.4+mc1.19.2-fabric-create-0.5.1.jar)**
- Remove Server Performance - Smooth Chunk Save
- Remove Better Compatibility Checker
- Remove Better Animal Models
- Remove Better Animals Plus
- Added Geckolib (geckolib-fabric-1.19-3.1.40.jar)
- **Added Naturalist (naturalist-fabric-3.0.4-1.19.2.jar)**

## 1.0.15
- **Update to Fabric 0.14.19**
- Update Applied Energistics 2: appliedenergistics2-fabric-12.9.2.jar -> appliedenergistics2-fabric-12.9.3.jar
- Update Architectury API (Fabric/Forge): architectury-6.5.69-fabric.jar -> architectury-6.5.82-fabric.jar
- Update Building Wands: BuildingWands_mc1.19.2-2.6.5_beta-fabric.jar -> BuildingWands-mc1.19.2-2.6.6-release-fabric.jar
- Update Carry On: carryon-fabric-1.19.2-2.0.4.6.jar -> carryon-fabric-1.19.2-2.0.5.11.jar
- Update CraftTweaker: CraftTweaker-fabric-1.19.2-10.1.39.jar -> CraftTweaker-fabric-1.19.2-10.1.44.jar
- Update Create Fabric: create-fabric-0.5.0.i-961+1.19.2.jar -> create-fabric-0.5.0.i-1017+1.19.2.jar
- Update Create Crafts & Additions: createaddition-fabric+1.19.2-20230211a.jar -> createaddition-fabric+1.19.2-20230508b.jar
- Update Dynmap-Forge/Fabric: Dynmap-3.5-beta-2-fabric-1.19.1.jar -> Dynmap-3.5-fabric-1.19.1.jar
- Update Extended Drawers: ExtendedDrawers-1.3.7+mc.1.19.2.jar -> ExtendedDrawers-1.4.0+mc.1.19.2.jar
- Update Immersive Aircraft [Fabric/Forge]: immersive_aircraft-0.4.1+1.19.2-fabric.jar -> immersive_aircraft-0.4.2+1.19.2-fabric.jar
- Update Magnum Torch: MagnumTorch-v4.2.1-1.19.2-Fabric.jar -> MagnumTorch-v4.2.2-1.19.2-Fabric.jar
- Update MemoryLeakFix: memoryleakfix-fabric-1.17+-1.0.0-beta.6.jar -> memoryleakfix-fabric-1.17+-1.0.0.jar
- Update Powah! (Rearchitected): Powah-4.0.6.jar -> Powah-4.0.10.jar
- Update Puzzles Lib: PuzzlesLib-v4.3.12-1.19.2-Fabric.jar -> PuzzlesLib-v4.4.0-1.19.2-Fabric.jar
- Update RightClickHarvest: right-click-harvest-3.1.0+1.19-1.19.2.jar -> right-click-harvest-3.2.0+1.19.x.jar
- Update Moonlight Lib: moonlight-1.19.2-2.2.27-fabric.jar -> moonlight-1.19.2-2.2.34-fabric.jar
- Update Simple Voice Chat: voicechat-fabric-1.19.2-2.3.28.jar -> voicechat-fabric-1.19.2-2.4.8.jar
- Update Small Ships [Fabric & Forge]: smallships-fabric-1.19.X-2.0.0a1.1.1.jar -> smallships-fabric-1.19.2-2.0.0a2.1.jar
- Update Supplementaries: supplementaries-1.19.2-2.2.64-fabric.jar -> supplementaries-1.19.2-2.3.11-fabric.jar
- Update Traveler's Backpack [Fabric]: travelers-backpack-1.19.2-8.2.17.jar -> travelers-backpack-1.19.2-8.2.22.jar
- Update Xaero's Minimap: Xaeros_Minimap_23.3.2_Fabric_1.19.1.jar -> Xaeros_Minimap_23.4.3_Fabric_1.19.1.jar
- Update Xaero's World Map: XaerosWorldMap_1.29.4_Fabric_1.19.1.jar -> XaerosWorldMap_1.30.2_Fabric_1.19.1.jar
- AddedCreate: Steam 'n' Rails (Steam_Rails-1.2.6+fabric-mc1.19.2.jar)
- Added CreateFabric&REIBugFix (CreateFabricREIBugFix-0.1.2-mc1.19.x.jar)

## 1.0.14
- **Update to Fabric 0.14.18**
- Update Balm (Fabric Edition): balm-fabric-4.5.2+0.jar -> balm-fabric-1.19.2-4.5.7.jar
- Update CraftTweaker: CraftTweaker-fabric-1.19.2-10.1.34.jar -> CraftTweaker-fabric-1.19.2-10.1.39.jar
- Update Enchanted Vertical Slabs: enchanted-vertical-slabs-1.6.jar -> MC-1.19-enchanted-vertical-slabs-1.6.2.jar
- Update Extended Drawers: ExtendedDrawers-1.3.6+mc.1.19.2.jar -> ExtendedDrawers-1.3.7+mc.1.19.2.jar
- Update Fabric API: fabric-api-0.73.2+1.19.2.jar -> fabric-api-0.76.0+1.19.2.jar
- Update Forge Config API Port: ForgeConfigAPIPort-v4.2.10-1.19.2-Fabric.jar -> ForgeConfigAPIPort-v4.2.11-1.19.2-Fabric.jar
- Update Immersive Aircraft [Fabric/Forge]: immersive_aircraft-0.3.0+1.19.2-fabric.jar -> immersive_aircraft-0.4.1+1.19.2-fabric.jar
- Update Inventory Essentials (Fabric Edition): inventoryessentials-fabric-1.19-5.0.0.jar -> inventoryessentials-fabric-1.19-5.0.2.jar
- Update Litematica: litematica-fabric-1.19.2-0.12.6.jar -> litematica-fabric-1.19.2-0.12.7.jar
- Update Lithium (Fabric): lithium-fabric-mc1.19.2-0.10.4.jar -> lithium-fabric-mc1.19.2-0.11.1.jar
- Update Macaw's Doors: mcw-doors-1.0.8fabric-mc1.19.2.jar -> mcw-doors-1.0.9fabric-mc1.19.2.jar
- Update MemoryLeakFix: memoryleakfix-1.19.3-0.7.0.jar -> memoryleakfix-fabric-1.17+-1.0.0-beta.6.jar
- Update Mod Menu: modmenu-4.1.2.jar -> modmenu-4.2.0-beta.2.jar
- Update Pick Up Notifier: PickUpNotifier-v4.2.0-1.19.2-Fabric.jar -> PickUpNotifier-v4.2.4-1.19.2-Fabric.jar
- Update Roughly Enough Items Fabric/Forge (REI): RoughlyEnoughItems-9.1.587.jar -> RoughlyEnoughItems-9.1.595.jar
- Update Moonlight Lib: moonlight-1.19.2-2.2.7-fabric.jar -> moonlight-1.19.2-2.2.27-fabric.jar
- Update Supplementaries: supplementaries-1.19.2-2.2.49-fabric.jar -> supplementaries-1.19.2-2.2.64-fabric.jar
- Update Traveler's Backpack [Fabric]: travelers-backpack-1.19.2-8.2.13.jar -> travelers-backpack-1.19.2-8.2.17.jar
- Update Xaero's Minimap: Xaeros_Minimap_23.1.0_Fabric_1.19.1.jar -> Xaeros_Minimap_23.3.2_Fabric_1.19.1.jar
- Update Xaero's World Map: XaerosWorldMap_1.28.9_Fabric_1.19.1.jar -> XaerosWorldMap_1.29.4_Fabric_1.19.1.jar

## 1.0.13
- Update Architectury API (Fabric/Forge): architectury-6.4.62-fabric.jar -> architectury-6.5.69-fabric.jar
- Update CraftTweaker: CraftTweaker-fabric-1.19.2-10.1.31.jar -> CraftTweaker-fabric-1.19.2-10.1.34.jar
- Update Create Fabric: create-fabric-0.5.0g-796+1.19.2.jar -> create-fabric-0.5.0.i-961+1.19.2.jar
- Update Create Crafts & Additions: createaddition+1.19.2-1.19.2-20221218a.jar -> createaddition-fabric+1.19.2-20230211a.jar
- Update Fabric API: fabric-api-0.73.0+1.19.2.jar -> fabric-api-0.73.2+1.19.2.jar
- Update Iris Shaders: iris-mc1.19.2-1.5.0.jar -> iris-mc1.19.2-1.5.2.jar
- Update Jade Addons (Fabric): JadeAddons-1.19.2-fabric-3.0.0.jar -> JadeAddons-1.19.2-fabric-3.1.0.jar
- Update Macaw's Doors: mcw-doors-1.0.7fabric-mc1.19.2.jar -> mcw-doors-1.0.8fabric-mc1.19.2.jar
- Update Moonlight Lib: moonlight-1.19.2-2.2.2-fabric.jar -> moonlight-1.19.2-2.2.7-fabric.jar
- Update Supplementaries: supplementaries-1.19.2-2.2.45-fabric.jar -> supplementaries-1.19.2-2.2.49-fabric.jar
- Update Traveler's Backpack [Fabric]: travelers-backpack-1.19.2-8.2.11.jar -> travelers-backpack-1.19.2-8.2.13.jar
- Update Xaero's Minimap: Xaeros_Minimap_22.17.1_Fabric_1.19.1.jar -> Xaeros_Minimap_23.1.0_Fabric_1.19.1.jar
- Update Xaero's World Map: XaerosWorldMap_1.28.8_Fabric_1.19.1.jar -> XaerosWorldMap_1.28.9_Fabric_1.19.1.jar

## 1.0.12
- **Update to Fabric 0.14.13**
- Update Building Wands: BuildingWands_mc1.19.2-2.6.4_release-fabric.jar -> BuildingWands_mc1.19.2-2.6.5_beta-fabric.jar
- Update Carry On: carryon-fabric-1.19.2-2.0.2.3.jar -> carryon-fabric-1.19.2-2.0.4.6.jar
- Update CraftTweaker: CraftTweaker-fabric-1.19.2-10.0.24.jar -> CraftTweaker-fabric-1.19.2-10.1.31.jar
- Update Dynamic Music Updated: dynmus-2.2.1+1.19.2-fabric.jar -> dynmus-2.2.2+1.19.2-fabric.jar
- Update Dynmap-Forge/Fabric: Dynmap-3.5-beta-1-fabric-1.19.1.jar -> Dynmap-3.5-beta-2-fabric-1.19.1.jar
- Update Extended Drawers: ExtendedDrawers-1.3.5+mc.1.19.2.jar -> ExtendedDrawers-1.3.6+mc.1.19.2.jar
- Update Fabric API: fabric-api-0.72.0+1.19.2.jar -> fabric-api-0.73.0+1.19.2.jar
- Update Immersive Aircraft [Fabric/Forge]: immersive_aircraft-0.2.0+1.19.2.jar -> immersive_aircraft-0.3.0+1.19.2-fabric.jar
- Update Jade 🔍: Jade-1.19.1-fabric-8.7.0.jar -> Jade-1.19.1-fabric-8.7.3.jar
- Update Login Protection[Forge/Fabric]: logprot-fabric-1.19-1.9.jar -> logprot-fabric-1.19-2.0.jar
- Update Magnum Torch: MagnumTorch-v4.2.0-1.19.2-Fabric.jar -> MagnumTorch-v4.2.1-1.19.2-Fabric.jar
- Update Roughly Enough Items Fabric/Forge (REI): RoughlyEnoughItems-9.1.580.jar -> RoughlyEnoughItems-9.1.587.jar
- Update Moonlight Lib: moonlight-1.19.2-2.1.25-fabric.jar -> moonlight-1.19.2-2.2.2-fabric.jar
- Update Simple Voice Chat: voicechat-fabric-1.19.2-2.3.26.jar -> voicechat-fabric-1.19.2-2.3.28.jar
- Update Supplementaries: supplementaries-1.19.2-2.2.35-fabric.jar -> supplementaries-1.19.2-2.2.45-fabric.jar
- Update Syncmatica: syncmatica-1.18.2-0.3.7.jar -> syncmatica-1.18.2-0.3.8.jar
- Update Traveler's Backpack [Fabric]: travelers-backpack-1.19.2-8.2.10.jar -> travelers-backpack-1.19.2-8.2.11.jar
- Update Trinkets (Fabric): trinkets-3.4.1.jar -> trinkets-3.4.2.jar
- Update Xaero's Minimap: Xaeros_Minimap_22.17.0_Fabric_1.19.1.jar -> Xaeros_Minimap_22.17.1_Fabric_1.19.1.jar
- Update Xaero's World Map: XaerosWorldMap_1.28.7_Fabric_1.19.1.jar -> XaerosWorldMap_1.28.8_Fabric_1.19.1.jar
- Update YetAnotherConfigLib: YetAnotherConfigLib-1.7.1.jar -> YetAnotherConfigLib-2.2.0-for-1.19.2.jar

## 1.0.11
- Updated Better Animals Plus: betteranimalsplus-1.19-11.0.7-fabric.jar -> betteranimalsplus-1.19.2-11.0.10-fabric.jar
- Updated CraftTweaker: CraftTweaker-fabric-1.19.2-10.0.23.jar -> CraftTweaker-fabric-1.19.2-10.0.24.jar
- Updated Dynamic Music Updated: dynmus-2.1.0+1.19.2-fabric.jar -> dynmus-2.2.1+1.19.2-fabric.jar
- Updated Fabric API: fabric-api-0.71.0+1.19.2.jar -> fabric-api-0.72.0+1.19.2.jar
- Updated Forge Config API Port: ForgeConfigAPIPort-v4.2.9-1.19.2-Fabric.jar -> ForgeConfigAPIPort-v4.2.10-1.19.2-Fabric.jar
- Updated Immersive Aircraft [Fabric/Forge]: immersive_aircraft-0.1.1+1.19.2.jar -> immersive_aircraft-0.2.0+1.19.2.jar
- Updated Jade 🔍: Jade-1.19.1-fabric-8.6.4.jar -> Jade-1.19.1-fabric-8.7.0.jar
- Updated JamLib: jamlib-0.5.0.jar -> jamlib-0.6.0+1.19.jar
- Updated RightClickHarvest: right-click-harvest-3.0.6.jar -> right-click-harvest-3.1.0+1.19-1.19.2.jar
- Updated Moonlight Lib: moonlight-1.19.2-2.1.22-fabric.jar -> moonlight-1.19.2-2.1.25-fabric.jar
- Updated Supplementaries: supplementaries-1.19.2-2.2.32b-fabric.jar -> supplementaries-1.19.2-2.2.35-fabric.jar
- Updated Traveler's Backpack [Fabric]: travelers-backpack-1.19.2-8.2.9.jar -> travelers-backpack-1.19.2-8.2.10.jar

## 1.0.10
- **Updated Fabric to 0.14.12**
- Updated Applied Energistics 2: appliedenergistics2-fabric-12.9.1.jar -> appliedenergistics2-fabric-12.9.2.jar
- Updated Building Wands: BuildingWands_mc1.19.2-2.6.3_release-fabric.jar -> BuildingWands_mc1.19.2-2.6.4_release-fabric.jar
- Updated Carry On: carryon-fabric-1.19.2-2.0.0.6.jar -> carryon-fabric-1.19.2-2.0.2.3.jar
- Updated Fabric API: fabric-api-0.69.0+1.19.2.jar -> fabric-api-0.71.0+1.19.2.jar
- Updated Iris Shaders: iris-mc1.19.2-1.4.5.jar -> iris-mc1.19.2-1.5.0.jar
- Updated Jade 🔍: Jade-1.19.1-fabric-8.6.3.jar -> Jade-1.19.1-fabric-8.6.4.jar
- Updated Litematica: litematica-fabric-1.19.2-0.12.5.jar -> litematica-fabric-1.19.2-0.12.6.jar
- Updated Macaw's Windows: mcw-windows-2.1.1-mc1.19.2fabric.jar -> mcw-windows-2.1.2-mc1.19.2fabric.jar
- Updated Moonlight Lib: moonlight-1.19.2-2.1.20-fabric.jar -> moonlight-1.19.2-2.1.22-fabric.jar
- Updated Simple Voice Chat: voicechat-fabric-1.19.2-2.3.24.jar -> voicechat-fabric-1.19.2-2.3.26.jar
- Updated Supplementaries: supplementaries-1.19.2-2.2.29-fabric.jar -> supplementaries-1.19.2-2.2.32b-fabric.jar
- Added Dynamic Music Updated

## 1.0.9
- Remove Mining Gadgets (buggy)
- Updated Moonlight Lib: moonlight-1.19.2-2.1.19-fabric.jar -> moonlight-1.19.2-2.1.20-fabric.jar
- Updated Simple Voice Chat: voicechat-fabric-1.19.2-2.3.23.jar -> voicechat-fabric-1.19.2-2.3.24.jar
- Updated Supplementaries: supplementaries-1.19.2-2.2.27-fabric.jar -> supplementaries-1.19.2-2.2.29-fabric.jar
- Added Iron Jetpacks Fabric
- Added Powah! (Rearchitected)
- Added Enchanted Vertical Slabs

## 1.0.8-s1
**Server only update**
- Fix Mining gadgets blank upgrade module recipe being empty

## 1.0.8
- Updated Macaw's Fences and Walls: mcw-fences-1.0.6fabric-mc1.19.2.jar -> mcw-fences-1.0.7-mc1.19.2fabric.jar
- Updated Macaw's Windows: mcw-windows-2.1.0fabric-1.19.2.jar -> mcw-windows-2.1.1-mc1.19.2fabric.jar
- Updated Xaero's World Map: XaerosWorldMap_1.28.6_Fabric_1.19.1.jar -> XaerosWorldMap_1.28.7_Fabric_1.19.1.jar
- Added Crafttweaker and receipe for Quiver (closes [issue#29](https://gitlab.com/thesociety/transportcraft-lite/-/issues/29))
- Added Create Crafts & Additions (closes [issue#15](https://gitlab.com/thesociety/transportcraft-lite/-/issues/15))

## 1.0.7
- Removed Bits and Chisel (crashes game)

## 1.0.6
- Added AdvancedChatCore (already only just adding the core mod, fixes links in chat not working)
- Added Immersive Aircraft
- Added Macaw's Doors
- Added Macaw's Windows
- Added Macaw's Fences and Walls
- Added Supplementaries

## 1.0.5
- Added Patchouli as it seems one of our mods need it (closes [issue#27](https://gitlab.com/thesociety/transportcraft-lite/-/issues/27))
- Updated Xaero's Minimap: Xaeros_Minimap_22.16.3_Fabric_1.19.1.jar -> Xaeros_Minimap_22.17.0_Fabric_1.19.1.jar
- Updated Xaero's World Map: XaerosWorldMap_1.28.4_Fabric_1.19.1.jar -> XaerosWorldMap_1.28.6_Fabric_1.19.1.jar
- Added Trinkets (closes [issue#28](https://gitlab.com/thesociety/transportcraft-lite/-/issues/28))
- Added AppleSkin
- Added Krypton
- Added Roughly Enough Resources
- Added Building Wands
- Replace Chisels & Bits - For Fabric with Bits And Chisels (latter is more popular and supposedly a lot better performance)
- Added Create Chunkloading

## 1.0.4
- Rebuilt the entire configuration folder, moved client configs to YOSBR to prevent overwriting user settings on update
- Configure Better Compatibility Report
- Replace WTHIT with Jade and Jade Addons (seems to work better)
- Added MemoryLeakFix
- Added LazyDFU
- Added Magnum Torch
- Added Carry On
- Added Farsight
- Added Login Protection
- Added Server Performance - Smooth Chunk Save
- Added Pick Up Notifier
- Replace DefaultOptions with Your Options Shall Be Respected (closes [issue#19](https://gitlab.com/thesociety/transportcraft-lite/-/issues/19))
- Enable voice chat group members to be heard locally
- Removed bindings by default for multiple key's that are not in use
- By default MiniHUD info lines are off and light overlay is mapped to F8
- By default minimap moved to top right corner of the map
- Map view key has been changed to Y as M is used by many mods (fixes [issue#17](https://gitlab.com/thesociety/transportcraft-lite/-/issues/17))
- Added Transportcraft server to serverlist automatically (if modpack launches the first time)

> It's reccomended to delete your options.txt and config folder and let this version to re-download now configured client configs.

## 1.0.3
- Added Simple Voice Chat

## 1.0.2
- Remove Bobby (caused worlds not to load)
- Added Syncmatica

## 1.0.1
- Added BetterF3
- Added Mod Menu
- Added Decorative Blocks
- Added Bobby
- Added Litematica
- Added Tweakeroo
- Added Carpet
- Updated FallingTree (Forge&Fabric): FallingTree-1.19.2-3.9.0.jar -> FallingTree-1.19.2-3.10.0.jar

## 1.0.0
- **Update to MC 1.19.2**
- **Completely modpack rebuild to run on Fabric instead of Forge**
> Check the full modlist to see mod changes

## 0.9.7
- **Update to Forge 36.2.33**
- Update to Applied Energetics 2 8.4.7
- Update to Bookshelf 10.4.31
- Update to CC: Tweaked 1.100.4
- Update to Chisel & Bits 1.0.63
- Update to CoFH COre 1.4.2.9
- Update to Dynamic Tress 0.10.0-Beta33
- Update to Dynamic Tress + 0.1.0-Beta11
- Update to Immersive Petroleum 3.3.0-11
- Update to Jade 2.8.1
- Update to JEI 7.7.1.152
- Update to Mekanism (and it's additions) 10.1.0.455
- Update to More Overlays Updated 1.18.18
- Update to Performant 5-3.90m
- Update to Gauges and Switches 1.2.14
- Update to Storage Drawers 8.5.1
- Update to Thermal Expansion 1.4.2.4
- Update to Thermal Foundation 1.4.3.10
- Update to Trash Cans 1.0.13
- Update to Travelers Backpack 5.4.7
- Update to Xaero's Minimap 22.3.1
- Update to Xaero's Worldmap 1.20.4
- Update to Dynmap-3.4-beta-1.1-forge-1.16.5

## 0.9.6
- **Update to Forge 36.2.22**
- Update to Applied Energetics 2 8.4.5
- Update to CC: Tweaked 1.99.1
- Update to Immersive Engineering 5.0.6-141
- Update to Immersive Petroleum 3.3.0-9
- Update to Mekanism 10.1.0.455
- Update to Mekanism Additions 10.1.0.455
- Update to Mekanism Generators 10.1.0.455
- Update to Mekanism Tools 10.1.0.455
- Update to Performant 5-3.81m
- Update to Ploymorph 0.37
- Update to MineColonies 1.0.309
- Update to Storage Drawers 8.4.0
- Update to Trashcans 1.0.11
- Update to Xaero's Minimap 21.22.5
- Update to Xaero's World Map 1.18.7

## 0.9.5
- **Update to Forge 36.2.20**
- Includes the critical Minecraft fix.
- Update to CoFH Core 1.4.0.6
- Update to More Overlays Updated 1.18.17
- Update to Thermal Expansion 1.4.1.3
- Update to Thermal Foundation 1.4.1.7
- Update to Thermal Innovation 1.4.1.3

## 0.9.4
- **Update to Forge 36.2.19**
- Update to Applied Energistics 2 8.4.4
- Update to Chisel & Bits 1.0.43
- Update to CC: Tweaked 1.99.0
- Update to CoFH Core 1.4.0.2
- Update to Create 0.3.2g
- Update to Dynamic Trees 0.10.0-Beta25
- Update to Flywhheel 0.2.5
- Update to MrCrayfish's Furniture Mod 7.0pre22
- Update to GraveStone Mod 1.0.7
- Update to Immersive Petroleum 3.3.0-8
- Update to Industrial Foregoing 3.2.14.7-16
- Update to Iron Chests 11.2.21
- Update to Just Enough Items 7.7.1.137
- Update to Mekanism 10.0.24.453
- Update to Mekanism Additions 10.0.24.453
- Update to Mekanism Generators 10.0.24.453
- Update to Mekanism Tools 10.0.24.453
- Update to MineColonies 1.0.205 BETA
- Update to More Overlays Updated 1.18.16
- Update to Obfuscate 0.6.2
- Update to Performant 5-3.79m
- Update to Polymorph 0.33
- Update to Gauges and Switches 1.2.12
- Update to Server Tab Info 1.3.4
- Update to Structurize 0.13.252
- Update to SuperMartijn642's Core Lib 1.0.15
- Update to TerraForged 0.2.16-BETA-2
- Fix duplicate Terraforged somehow
- Update to Thermal Cultivation 1.4.1.3
- Update to Thermal Expansion 1.4.1.2
- Update to Thermal Foundation 1.4.1.5
- Update to Thermal Innovation 1.4.1.2
- Update to Thermal Locomotion 1.4.1.2
- Update to Titanium 3.2.8.7-22
- Update to Traveler's Backpack 5.4.5
- Update to Xaero's Minimap 21.22.3
- Update to Xaero's World Map 1.18.6

### Server only
- Update to Dynmap 3.3-beta-1

## 0.9.3
- **Update to Forge 36.2.8**
- Update to Applied Energetics 2 8.4.3
- Update to Better Dungeons 1.2.1
- Update to Building Gadgets 3.8.2
- Update to Create 0.3.2f
- Update to Immersive Engineering 5.0.5-140
- Update to Immersive Petroleum 3.3.0-7
- Update to Mekanism (and it's submods) 10.0.22.449
- Update to Xaero's Minimap 21.20.0
- Update to Xaero's World Map 1.18.0

## 0.9.2
- **Update to Forge 36.2.6**
- Update to Alex's Mobs 1.12.1
- Update to Bookshelf 10.3.29
- Update to Jade 2.8.0
- Update to Chisel & Bits Beta 1.0.7
- Add CC: Tweaked ([issue#13](https://gitlab.com/thesociety/transportcraft-lite/-/issues/13))
- Add SlimeDetector ([issue#12](https://gitlab.com/thesociety/transportcraft-lite/-/issues/12))

#### Server only
- Update to Dynmap 3.2-beta-3

## 0.9.1
- **Update to Forge 36.2.5**
- Remove Falling Tress ([issue#3](https://gitlab.com/thesociety/transportcraft-lite/-/issues/3))
- Add Alex's Mobs ([issue#2](https://gitlab.com/thesociety/transportcraft-lite/-/issues/2))
- Add Chisel & Bits ([issue#5](https://gitlab.com/thesociety/transportcraft-lite/-/issues/5))
- Add Dynamic Surroundings ([issue#6](https://gitlab.com/thesociety/transportcraft-lite/-/issues/6))
- Add Polymorph ([issue#8](https://gitlab.com/thesociety/transportcraft-lite/-/issues/8))
- Add Minecolonies ([issue#9](https://gitlab.com/thesociety/transportcraft-lite/-/issues/9)) 
- Add Small Ships ([issue#10](https://gitlab.com/thesociety/transportcraft-lite/-/issues/10))

**Server updates**
- Remove FTB Backups ([issue#7](https://gitlab.com/thesociety/transportcraft-lite/-/issues/7))

## 0.9.0
- Added Create
- Added Dynamic Trees
- Added Dynamic Trees+
- Added Terraforged
- Added YUNG's Better Caves
- Added YUNG's Better Mineshafts
- Added YUNG's Better Dungeons
- Added YUNG's Better Strongholds
- Update to Applied Energetics 8.4.1
- Update to Immersive Petroleum 3.3.0-6

## 0.8.4
- **Update to Forge 36.2.4**
- Update to Immersive Enginerring 5.0.3-138
- Update to Immersive Railroading 1.9.1
- Update to Industrial Foregoing 3.2.14.6-14
- Update to Jade 2.7.4
- Update to Torchmaster 2.3.8
- Update to Xaero's Minimap 21.16.0
- Update to Xaero's World Map 1.17.0.1
- Remove ReAuth

## 0.8.3
- **Update to Forge 36.2.2**
- Update to Immersive Railroading 1.9.0
- Update to Iron Chests 11.2.13
- Update to Performant 5-3.72
- Update to Storage Srawers 8.3.0
- Update to Thermal Cultivation 1.3.0
- Update to Thermal Expansion 1.3.0
- Update to Thermal Foundation 1.3.0
- Update to Thermal Foundation 1.3.2
- Update to Thermal Innovation 1.3.0
- Update to Thermal Locomotion 1.3.0
- Update to Xaero's Minmap 21.14.1
- Update to Xaero's World 1.16.0

## 0.8.2
- **Update to Forge 36.2.1**
- Update to Gravestone 1.0.6
- Update to Immersive Engineering to 5.0.2-137
- Update to Immersive Petroleum to 3.3.0-5
- Update to Industrial Foregoing 3.2.14.3-9
- Update to More Overlays 1.18.15
- Update to Performant 5-3.71m
- Update to RS Gauges 1.2.11
- Update to Xaero's Minimap 21.14.0
- Update to Xaero's World Map 1.15.0.1

## 0.8.1
- **Update to Forge 36.1.36**
- Update Applied Energistics 2 to 8.4.0
- Update Default Options to 12.2.1
- Update Immersive Engineering to 5.0.0-135
- Update Immersive Petroleum to 3.3.0-4
- Update Industrial Foregoing to 3.2.13.1-5
- Update More Overlays to 1.18.14
- Update Performant to 5.3.66
- Update Xaero's Minimap to 21.10.0.3
- Add Inventory Tweaks Renewed 1.0.0
- Remove Inventory Sorter

## 0.8.0
- **Update to Minecraft 1.16.5**
- **Update to Forge 36.1.24**
- Update Applied Energistics 2 to 8.3.1
- Update Bookshelf to 1.0.8
- Update Building Gadgets to 3.8.0
- Update Cloth Config to 4.11.26
- Update FallingTree to 2.11.1
- Update MrCrayfish's Furniture Mode to 7.0.0-pre20
- Update GraveStone Mod to 1.16.5-1.0.4
- Update Immersive Engineering to 4.2.4-134
- Update Immersive Petroleum to 3.2.0-3
- Update Industrial Foregoing to 3.2.11-55cb112
- Update JEI to 7.7.0.99
- Update JEI Integration to 7.0.1.15
- Update Mekanism to 10.0.21.448
- Update Mekanism Additions to 10.0.21.448
- Update Mekanism Generators to 10.0.21.448
- Update Mekanism Tools to 10.0.21.448
- Update Mining Gadgets to 1.7.5
- Update More OVerlays to 1.18.13
- Update Obfuscate to 1.16.5
- Update Performant to 1.16.2-5.3.60m
- Update Gauges and Switches to 1.2.10
- Update Torchmaster to 2.3.7
- Update Trashcans to 1.0.10
- Update Traveler's Backpack to 5.4.2
- Update Xaero's Minimap to 21.8.2
- Update Xaero's World Map to 1.14.1
- Remove Hywla (abandoned)
- Remove WAWLA (depended on Hywla)
- Add Jade (replacement for Hywla + addons)

**Server only**
- Update dynmap to 3.1 stable
- Update Morpheus to 4.2.70
## 0.7.7
- Update Astikorcards to 1.1.1
- Update Cloth Config API to 4.1.19
- Update COFH Core to 1.2.1
- Update Performant to 5.3.40m
- Update Thermal Cultivation to 1.2.0
- Update Thermal Expansion to 1.2.0
- Update Thermal Foundation to 1.2.0
- Update Thermal Innovation to 1.2.0
- Update Thermal Locomotion to 1.2.0
- Update Titanium to 3.2.8.2
- Update Trashcans to 1.0.9
- Update Xaeros Minimap to 21.6.0
- Update Xaeros World Map 1.13.2

## 0.7.6
- Add Performant 3.30m
- Update Applied Energetics to 8.2.0
- Update Cloth Config API to 4.1.14
- Update Gravestone to 1.0.9
- Update Mekanism to 10.0.19.446
- Update Mekanism Additions to 10.0.19.446
- Update Mekanism Generators to 10.0.19.446
- Update Mekanism Tools to 10.0.19.446
- Update Mouse Tweaks to 2.14
- Update Storage Drawers to 8.2.2
- Update Titanium to 3.2.7
- Update Trashcans to 1.0.8
- Update WAWL to 7.0.4
- Update Xaeros Minimap to 21.4.0
- Update Xaeros World Map to 1.16.5

## 0.7.5
- **Update to Forge 35.1.37**
- Update Bookshelf to 9.3.25
- Update Falling Tree to 2.10.0
- Update Industrial Foregogin to 3.2.8.2-6b5c95b
- Update Titanium to 3.2.5
- Update Trashcasns to 1.0.6a
- Update Xaero's Minimap to 21.0.0
- Update Xaero's World Map to 1.11.7

## 0.7.4
- **Update to Forge 35.1.29**
- Update Cloth config to 4.1.3
- Update COFH Core to 1.1.6
- Update Gravestone to 1.0.8
- Update Industrial Foregoing to 3.2.7-7d95990
- Update Thermal Cultivation 1.1.6
- Update Thermal Expansion 1.0.5
- Update Thermal Expansion to 1.1.16
- Update Thermal Innovation to 1.1.7
- Update Thermal Locomotion to 1.1.6
- Update Thermal Foundation to 1.1.6
- Update Titanium to 3.2.4
- Update Trashcans to 1.0.6
- Update Xaeros Minimap to 20.30.1
- Update Xaeros World Map to 1.11.6
- [Server] Update Dynmap to 3.1-beta6-forge-1.16.3

## 0.7.3
- Update Building Gadgets to 3.7.3
- Update FallingTree to 2.8.1
- Update Gravestone to 1.0.7
- Update Immersive Engienering to 4.1.2-129
- Update Immersive Petroleum to 3.1.0-2
- Update RS Gauges to 1.2.6
- Update Trashcans to 1.0.5
- Update UniversalModCore to 1.0.1
- Update Xaero's Minimap to 20.29.1
- Disable Mekanism's baby mobs

## 0.7.2
- Update Xaero's Minimap

## 0.7.1
- Readd FallingTree that was lost with 1.16 upgrade

## 0.7.0
- Update to MC 1.16.4
- Update to Forge 35.1.13
- Remove Journeymap (latest supported version 1.15)
- Remove Performant (latest supported version 1.15)
- Re-add Immersive Petroleum
- Re-add Immersive Railroading
- Add Xaero's Minimap
- Add Xaero's World Map

## 0.6.5
- Update Forge to 31.2.46
- Update Performant

## 0.6.4
- Update Applied Energetics 2
- Update Building Gadgets
- Update CC: Tweaked
- Update CoFH Core
- Update Immersive Engineering
- Update Performant
- Update Storage Drawers
- Update Thermal Cultivation
- Update Thermal Expansion
- Update Thermal Innovation
- Update Thermal Locomotion
- Update Thermal Foundation

## 0.6.3
- Add Mekanism
- Add Mekanism Additions
- Add Mekanism Generators
- Add Mekanism Tools
- Remove Refined Storage
- Remove Refined Storage Addons
- Add Applied Energistics 2
- Add Gauges and Switches
- Add Traveler's Backpack

## 0.6.2
- Add Thermal Cultivation
- Add Thermal Expansion
- Add Thermal Foundation 
- Add Thermal Innovation 
- Add Thermal Locomotion 

## 0.6.1
- Remove Immersive Petroleum (not compatible with 1.15)
- Remove Immersive Railroading and all resourcepacks (not compatible with 1.15)
- Update to Minecraft 1.15
- Update all mods to their 1.15 counterparts
- Replaced More Overlays with More Overlays Updated
- Add MrCrayfish's Vehicles Mod

## 0.5.1
- Update FallingTree to 2.4.0

## 0.5
- Add Immersive Petroleum
- Add FTB Backups
- Add Trash Cans mod

## 0.4.1
- Add Performant mod

## 0.4
- Add Mining Gadgets mod
- Update more overlays config

## 0.3.2
- Remove MrCrayfish Vehicles mod (reason: [Github #345](https://github.com/MrCrayfish/MrCrayfishVehicleMod/issues/345))

## 0.3.1
- Reverse sneaking logic for fallen trees mod (only cut trees if sneaking enabled)
- Add More Overlays

## 0.3
- Add Falling Tree mod
- Add ReAuth mod
- Add Morpheus server-side mod

## 0.2

- Set default setting: UI scale to 2
- Set default resourcepacks: Enable all IR related packs (won't work on first launch, but will work on second)
